# Garbage-Collection-Simulator

In diesem Repository befindet sich der Simulator, der im Rahmen meiner Masterarbeit **Garbage-Collection-Algorithmen und ihre Simulation** implementiert wurde.
Er steht hier unter der [Apache-2.0-Lizenz](https://github.com/phist91/gcsim/blob/master/LICENSE.md) zur freien Verwendung zur Verfügung.
Das Repository enthält folgende Dateien und Ordner:

* `src`: Java-Quellcode-Dateien
* `test`: Quellcode-Dateien für JUnit-Tests
* `doc`: Javadoc-Dateien
* `pom.xml`: Maven Project Object Model

## Download und Verwendung des Simulators
Der Simulator kann hier heruntergeladen werden: https://gitlab.com/phist91/gcsim/-/jobs/artifacts/master/browse?job=package

Der Start der Anwendung erfolgt in drei verschiedenen Möglichkeiten:
1. Ausführung der Datei `gcsim-1.0.1-full.jar`.
2. Ausführung in der Konsole mittels `java -jar gcsim-1.0.1-full.jar`.
3. Klonen des Repositorys und Ausführung eines Maven-Build-Zyklus mit `mvn package`.
Anschließend kann der Simulator mit `mvn exec:exec` gestartet werden.

Die Bedienung des Simulators wird in Kapitel 8 meiner Masterarbeit beschrieben, welche in [diesem Repository](https://gitlab.com/phist91/gc-thesis) zu finden ist.

Phil Steinhorst, 2019/09/02
