/*
 * Copyright 2019 Phil Steinhorst
 *
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package pst.gcsim.GarbageCollectors;
import org.junit.jupiter.api.Test;
import pst.gcsim.Allocators.NaiveAllocator;
import pst.gcsim.Controllers.CollectionController;
import pst.gcsim.Objects.HeapObject;
import pst.gcsim.Settings;

import java.util.ArrayList;

import static org.junit.jupiter.api.Assertions.*;

/**
 * Testklasse für {@code MarkSweep}.
 *
 * @author Phil Steinhorst
 */
class MarkSweepTest {
	
	/**
	 * Beispiel aus Abbildung 2.2.
	 * Test der Methode ohne GUI-Anbindung.
	 */
	@Test
	void markSweepTest() {
		MarkSweep<NaiveAllocator> ms = new MarkSweep<>(null);
		CollectionController<NaiveAllocator> controller = new CollectionController<>(
				true, new NaiveAllocator(32), ms);
		
		NaiveAllocator alloc = controller.getAllocator();
		HeapObject h = alloc.newObject(3);     // 0 -- 2, verwaist
		HeapObject a = alloc.newObject(2);     // 3 -- 4
		HeapObject b = alloc.newObject(4);     // 5 -- 8
		HeapObject g = alloc.newObject(5);     // 9 -- 13, verwaist
		HeapObject c = alloc.newObject(3);     // 14 -- 16
		HeapObject d = alloc.newObject(1);     // 17
		HeapObject j = alloc.newObject(8);     // 18 -- 25, verwaist
		HeapObject e = alloc.newObject(2);     // 26 -- 27
		HeapObject f = alloc.newObject(4);     // 28 -- 31
		
		a.getPointers().add(c);
		b.getPointers().add(c);
		b.getPointers().add(f);
		c.getPointers().add(d);
		c.getPointers().add(f);
		d.getPointers().add(c);
		d.getPointers().add(e);
		g.getPointers().add(d);
		g.getPointers().add(f);
		g.getPointers().add(j);
		h.getPointers().add(e);
		h.getPointers().add(h);
		h.getPointers().add(j);
		j.getPointers().add(g);
		
		ArrayList<HeapObject> roots = controller.getRoots();
		roots.add(a);
		roots.add(b);
		ArrayList<HeapObject> objects = controller.getObjects();
		objects.add(a);
		objects.add(b);
		objects.add(c);
		objects.add(d);
		objects.add(e);
		objects.add(f);
		objects.add(g);
		objects.add(h);
		objects.add(j);
		
		ms.markStart();
		
		// Prüfe korrekte Markierung
		assertEquals(MarkSweep.MS_BLACK, a.getMark());
		assertEquals(MarkSweep.MS_BLACK, b.getMark());
		assertEquals(MarkSweep.MS_BLACK, c.getMark());
		assertEquals(MarkSweep.MS_BLACK, d.getMark());
		assertEquals(MarkSweep.MS_BLACK, e.getMark());
		assertEquals(MarkSweep.MS_BLACK, f.getMark());
		assertEquals(MarkSweep.MS_WHITE, g.getMark());
		assertEquals(MarkSweep.MS_WHITE, h.getMark());
		assertEquals(MarkSweep.MS_WHITE, j.getMark());
		
		ms.sweep();
		
		// Prüfe korrekte Löschung aus objects
		assertTrue(objects.contains(a));
		assertTrue(objects.contains(b));
		assertTrue(objects.contains(c));
		assertTrue(objects.contains(d));
		assertTrue(objects.contains(e));
		assertTrue(objects.contains(f));
		assertFalse(objects.contains(g));
		assertFalse(objects.contains(h));
		assertFalse(objects.contains(j));
		
		// Prüfe Zurücksetzung der Markierung
		assertEquals(MarkSweep.MS_WHITE, a.getMark());
		assertEquals(MarkSweep.MS_WHITE, b.getMark());
		assertEquals(MarkSweep.MS_WHITE, c.getMark());
		assertEquals(MarkSweep.MS_WHITE, d.getMark());
		assertEquals(MarkSweep.MS_WHITE, e.getMark());
		assertEquals(MarkSweep.MS_WHITE, f.getMark());
		
		// Prüfe Belegung des Heaps
		boolean[] bitmap = {
				false, false, false,
				true, true,
				true, true, true, true,
				false, false, false, false, false,
				true, true, true,
				true,
				false, false, false, false, false, false, false, false,
				true, true,
				true, true, true, true
		};
		assertArrayEquals(bitmap, alloc.getAllocationBitmap().getBitmap());
	}
	
	/**
	 * Beispiel aus Abbildung 2.2.
	 * Test der Methode mit GUI-Anbindung.
	 */
	@Test
	void markSweepTimedTest() {
		Settings.ANIMATION_SPEED = 1;
		MarkSweep<NaiveAllocator> ms = new MarkSweep<>(null);
		CollectionController<NaiveAllocator> controller = new CollectionController<>(
				true, new NaiveAllocator(32), ms);
		
		NaiveAllocator alloc = controller.getAllocator();
		HeapObject h = alloc.newObject(3);     // 0 -- 2, verwaist
		HeapObject a = alloc.newObject(2);     // 3 -- 4
		HeapObject b = alloc.newObject(4);     // 5 -- 8
		HeapObject g = alloc.newObject(5);     // 9 -- 13, verwaist
		HeapObject c = alloc.newObject(3);     // 14 -- 16
		HeapObject d = alloc.newObject(1);     // 17
		HeapObject j = alloc.newObject(8);     // 18 -- 25, verwaist
		HeapObject e = alloc.newObject(2);     // 26 -- 27
		HeapObject f = alloc.newObject(4);     // 28 -- 31
		
		a.getPointers().add(c);
		b.getPointers().add(c);
		b.getPointers().add(f);
		c.getPointers().add(d);
		c.getPointers().add(f);
		d.getPointers().add(c);
		d.getPointers().add(e);
		g.getPointers().add(d);
		g.getPointers().add(f);
		g.getPointers().add(j);
		h.getPointers().add(e);
		h.getPointers().add(h);
		h.getPointers().add(j);
		j.getPointers().add(g);
		
		ArrayList<HeapObject> roots = controller.getRoots();
		roots.add(a);
		roots.add(b);
		ArrayList<HeapObject> objects = controller.getObjects();
		objects.add(a);
		objects.add(b);
		objects.add(c);
		objects.add(d);
		objects.add(e);
		objects.add(f);
		objects.add(g);
		objects.add(h);
		objects.add(j);
		
		ms.collect(true);
		// Workaround wegen Nebenläufigkeit des Timers...
		while (ms.isWorking()) {
			try {
				Thread.sleep(100);
			} catch (InterruptedException ignored) {
			}
		}
		
		// Prüfe korrekte Löschung aus objects
		assertTrue(objects.contains(a));
		assertTrue(objects.contains(b));
		assertTrue(objects.contains(c));
		assertTrue(objects.contains(d));
		assertTrue(objects.contains(e));
		assertTrue(objects.contains(f));
		assertFalse(objects.contains(g));
		assertFalse(objects.contains(h));
		assertFalse(objects.contains(j));
		
		// Prüfe Zurücksetzung der Markierung
		assertEquals(MarkSweep.MS_WHITE, a.getMark());
		assertEquals(MarkSweep.MS_WHITE, b.getMark());
		assertEquals(MarkSweep.MS_WHITE, c.getMark());
		assertEquals(MarkSweep.MS_WHITE, d.getMark());
		assertEquals(MarkSweep.MS_WHITE, e.getMark());
		assertEquals(MarkSweep.MS_WHITE, f.getMark());
		
		// Prüfe Belegung des Heaps
		boolean[] bitmap = {
				false, false, false,
				true, true,
				true, true, true, true,
				false, false, false, false, false,
				true, true, true,
				true,
				false, false, false, false, false, false, false, false,
				true, true,
				true, true, true, true
		};
		assertArrayEquals(bitmap, alloc.getAllocationBitmap().getBitmap());
	}
	
}