/*
 * Copyright 2019 Phil Steinhorst
 *
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package pst.gcsim.GarbageCollectors;

import org.junit.jupiter.api.Test;
import pst.gcsim.Allocators.SemispaceAllocator;
import pst.gcsim.Controllers.CollectionController;
import pst.gcsim.Objects.HeapObject;

import java.util.ArrayList;

import static org.junit.jupiter.api.Assertions.*;

/**
 * Testklasse für {@code SemispaceCollector}.
 *
 * @author Phil Steinhorst
 */
class SemispaceCollectorTest {
	/**
	 * Beispiel aus Abbildung 4.5.
	 * Test der Methode ohne GUI-Anbindung.
	 */
	@Test
	void collectTest() {
		SemispaceCollector<SemispaceAllocator> sc = new SemispaceCollector<>(null);
		CollectionController<SemispaceAllocator> controller = new CollectionController<>(
				true, new SemispaceAllocator(40), sc);
		
		SemispaceAllocator alloc = controller.getAllocator();
		HeapObject a = alloc.newObject(2);     // 0 -- 1, verwaist
		HeapObject b = alloc.newObject(3);     // 2 -- 4 --> 20 -- 22
		HeapObject c = alloc.newObject(2);     // 5 -- 6 --> 28 -- 29
		HeapObject d = alloc.newObject(4);     // 7 -- 10, verwaist
		HeapObject e = alloc.newObject(3);     // 11 -- 13 --> 30 -- 32
		HeapObject f = alloc.newObject(2);     // 14 -- 15 --> 23 -- 24
		HeapObject g = alloc.newObject(3);     // 16 -- 18 --> 25 -- 27
		
		b.getPointers().add(f);
		b.getPointers().add(g);
		d.getPointers().add(a);
		f.getPointers().add(c);
		f.getPointers().add(e);
		g.getPointers().add(f);
		
		ArrayList<HeapObject> roots = controller.getRoots();
		roots.add(b);
		ArrayList<HeapObject> objects = controller.getObjects();
		objects.add(a);
		objects.add(b);
		objects.add(c);
		objects.add(d);
		objects.add(e);
		objects.add(f);
		objects.add(g);
		
		sc.collect();
		
		// Prüfe korrekte Löschung aus objects
		assertFalse(objects.contains(a));
		assertTrue(objects.contains(b));
		assertTrue(objects.contains(c));
		assertFalse(objects.contains(d));
		assertTrue(objects.contains(e));
		assertTrue(objects.contains(f));
		assertTrue(objects.contains(g));
		
		// Prüfe Zurücksetzung der Markierung
		assertEquals(SemispaceCollector.UNDISCOVERED, b.getMark());
		assertEquals(SemispaceCollector.UNDISCOVERED, c.getMark());
		assertEquals(SemispaceCollector.UNDISCOVERED, e.getMark());
		assertEquals(SemispaceCollector.UNDISCOVERED, f.getMark());
		assertEquals(SemispaceCollector.UNDISCOVERED, g.getMark());
		
		// Prüfe neue Speicheradressen
		assertEquals(20, b.getAddress());
		assertEquals(28, c.getAddress());
		assertEquals(30, e.getAddress());
		assertEquals(23, f.getAddress());
		assertEquals(25, g.getAddress());
		
		// Prüfe Belegung des Heaps
		boolean[] bitmap = {
				false, false, false, false, false, false, false, false, false, false,
				false, false, false, false, false, false, false, false, false, false,
				true, true, true, true, true, true, true, true, true, true,
				true, true, true, false, false, false, false, false, false, false
		};
		assertArrayEquals(bitmap, alloc.getAllocationBitmap().getBitmap());
	}
	
	/**
	 * Beispiel aus Abbildung 4.5.
	 * Test der Methode mit GUI-Anbindung.
	 */
	@Test
	void collectTimedTest() {
		SemispaceCollector<SemispaceAllocator> sc = new SemispaceCollector<>(null);
		CollectionController<SemispaceAllocator> controller = new CollectionController<>(
				true, new SemispaceAllocator(40), sc);
		
		SemispaceAllocator alloc = controller.getAllocator();
		HeapObject a = alloc.newObject(2);     // 0 -- 1, verwaist
		HeapObject b = alloc.newObject(3);     // 2 -- 4 --> 20 -- 22
		HeapObject c = alloc.newObject(2);     // 5 -- 6 --> 28 -- 29
		HeapObject d = alloc.newObject(4);     // 7 -- 10, verwaist
		HeapObject e = alloc.newObject(3);     // 11 -- 13 --> 30 -- 32
		HeapObject f = alloc.newObject(2);     // 14 -- 15 --> 23 -- 24
		HeapObject g = alloc.newObject(3);     // 16 -- 18 --> 25 -- 27
		
		b.getPointers().add(f);
		b.getPointers().add(g);
		d.getPointers().add(a);
		f.getPointers().add(c);
		f.getPointers().add(e);
		g.getPointers().add(f);
		
		ArrayList<HeapObject> roots = controller.getRoots();
		roots.add(b);
		ArrayList<HeapObject> objects = controller.getObjects();
		objects.add(a);
		objects.add(b);
		objects.add(c);
		objects.add(d);
		objects.add(e);
		objects.add(f);
		objects.add(g);
		
		sc.collect(true);
		// Workaround wegen Nebenläufigkeit des Timers...
		while (sc.isWorking()) {
			try {
				Thread.sleep(100);
			} catch (InterruptedException ignored) {
			}
		}
		
		// Prüfe korrekte Löschung aus objects
		assertFalse(objects.contains(a));
		assertTrue(objects.contains(b));
		assertTrue(objects.contains(c));
		assertFalse(objects.contains(d));
		assertTrue(objects.contains(e));
		assertTrue(objects.contains(f));
		assertTrue(objects.contains(g));
		
		// Prüfe Zurücksetzung der Markierung
		assertEquals(SemispaceCollector.UNDISCOVERED, b.getMark());
		assertEquals(SemispaceCollector.UNDISCOVERED, c.getMark());
		assertEquals(SemispaceCollector.UNDISCOVERED, e.getMark());
		assertEquals(SemispaceCollector.UNDISCOVERED, f.getMark());
		assertEquals(SemispaceCollector.UNDISCOVERED, g.getMark());
		
		// Prüfe neue Speicheradressen
		assertEquals(20, b.getAddress());
		assertEquals(28, c.getAddress());
		assertEquals(30, e.getAddress());
		assertEquals(23, f.getAddress());
		assertEquals(25, g.getAddress());
		
		// Prüfe Belegung des Heaps
		boolean[] bitmap = {
				false, false, false, false, false, false, false, false, false, false,
				false, false, false, false, false, false, false, false, false, false,
				true, true, true, true, true, true, true, true, true, true,
				true, true, true, false, false, false, false, false, false, false
		};
		assertArrayEquals(bitmap, alloc.getAllocationBitmap().getBitmap());
	}
}
