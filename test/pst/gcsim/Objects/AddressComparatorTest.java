/*
 * Copyright 2019 Phil Steinhorst
 *
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package pst.gcsim.Objects;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotEquals;

/**
 * Testet den AddressComparator.
 *
 * @author Phil Steinhorst
 */
class AddressComparatorTest {
	
	/**
	 * Testet die {@code compare}-Methode.
	 */
	@Test
	void compareTest() {
		AddressComparator comp = new AddressComparator();
		HeapObject obj1 = new HeapObject(12, 24);
		HeapObject obj2 = new HeapObject(12, 12);
		HeapObject obj3 = new HeapObject(6, 32);
		
		assertEquals(1, comp.compare(obj1, obj3));
		assertEquals(-1, comp.compare(obj3, obj2));
		
		// Vergleich mit sich selbst
		assertEquals(0, comp.compare(obj1, obj1));
		assertEquals(0, comp.compare(obj2, obj2));
		assertEquals(0, comp.compare(obj3, obj3));
		
		// gleiche Adresse, aber verschiedene Objekte
		assertNotEquals(0, comp.compare(obj1, obj2));
	}
}