/*
 * Copyright 2019 Phil Steinhorst
 *
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package pst.gcsim.Allocators;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

/**
 * Testklasse für {@code AllocationBitmap}.
 *
 * @author Phil Steinhorst
 */
class AllocationBitmapTest {
	/**
	 * Teste Umgang mit ungültiger Länge.
	 */
	@Test
	void illegalLength() {
		assertThrows(IllegalArgumentException.class, () -> new AllocationBitmap(0));
		assertThrows(IllegalArgumentException.class, () -> new AllocationBitmap(-42));
	}
	
	/**
	 * Teste nextFree-Methode.
	 */
	@Test
	void nextFree() {
		boolean[] bitmap = {
				true, true, true, false, false, false, false, true,
				true, false, false, true, false, false, true, true
		};
		AllocationBitmap ab1 = new AllocationBitmap(bitmap);
		
		assertThrows(IllegalArgumentException.class, () -> ab1.nextFree(16));
		assertEquals(3, ab1.nextFree(0));
		assertEquals(5, ab1.nextFree(5));
		assertEquals(9, ab1.nextFree(7));
		assertEquals(-1, ab1.nextFree(14));
	}
	
	/**
	 * Teste nextUsed-Methode.
	 */
	@Test
	void nextUsed() {
		boolean[] bitmap = {
				true, true, true, false, false, false, false, true, true, false, false, true, false, false
				, true, false
		};
		AllocationBitmap ab1 = new AllocationBitmap(bitmap);
		
		assertThrows(IllegalArgumentException.class, () -> ab1.nextUsed(16));
		assertEquals(7, ab1.nextUsed(3));
		assertEquals(1, ab1.nextUsed(1));
		assertEquals(11, ab1.nextUsed(9));
		assertEquals(-1, ab1.nextUsed(15));
	}
}