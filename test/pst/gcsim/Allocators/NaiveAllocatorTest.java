/*
 * Copyright 2019 Phil Steinhorst
 *
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package pst.gcsim.Allocators;
import org.junit.jupiter.api.Test;
import pst.gcsim.Objects.HeapObject;

import static org.junit.jupiter.api.Assertions.*;

/**
 * Testklasse für {@code NaiveAllocator}.
 *
 * @author Phil Steinhorst
 */
class NaiveAllocatorTest {
	/**
	 * Teste Übergabe ungültiger Parameter an newObject
	 */
	@Test
	void newException() {
		NaiveAllocator alloc = new NaiveAllocator(16);
		assertThrows(IllegalArgumentException.class, () -> alloc.newObject(0));
		assertThrows(IllegalArgumentException.class, () -> alloc.newObject(-42));
	}
	
	/**
	 * Teste Allokation mit passender Lücke am Heapanfang
	 */
	@Test
	void allocateTest1() {
		boolean[] bitmap = {
				false, false, false, true, true, false, false, true,
				true, true, false, false, false, false, true, true
		};
		NaiveAllocator alloc = new NaiveAllocator(bitmap, 0);
		HeapObject result = alloc.newObject(3);
		
		boolean[] bitmap2 = {
				true, true, true, true, true, false, false, true,
				true, true, false, false, false, false, true, true
		};
		assertArrayEquals(bitmap2, alloc.getAllocationBitmap().getBitmap());        // Prüfe neue Bitmap
		assertEquals(3, alloc.getPosition());       // Prüfe neue Position des Allokators
		assertEquals(0, result.getAddress());       // Prüfe Adresse des erzeugten Objekts
		assertEquals(3, result.getSize());          // Prüfe Größe des neuen Objekts
	}
	
	/**
	 * Teste Allokation bei zu kleiner Lücke am Heapanfang
	 */
	@Test
	void allocateTest2() {
		boolean[] bitmap = {
				false, false, false, true, true, false, false, true,
				true, true, false, false, false, false, true, true
		};
		NaiveAllocator alloc = new NaiveAllocator(bitmap, 0);
		HeapObject result = alloc.newObject(4);
		
		boolean[] bitmap2 = {
				false, false, false, true, true, false, false, true,
				true, true, true, true, true, true, true, true
		};
		assertArrayEquals(bitmap2, alloc.getAllocationBitmap().getBitmap());
		assertEquals(14, alloc.getPosition());
		assertEquals(10, result.getAddress());
		assertEquals(4, result.getSize());
	}
	
	/**
	 * Teste Allokation bei passender Lücke hinter position
	 */
	@Test
	void allocateTest3() {
		boolean[] bitmap = {
				false, false, false, true, true, false, false, true,
				true, true, false, false, false, false, true, true
		};
		NaiveAllocator alloc = new NaiveAllocator(bitmap, 2);
		HeapObject result = alloc.newObject(3);
		
		boolean[] bitmap2 = {
				false, false, false, true, true, false, false, true,
				true, true, true, true, true, false, true, true
		};
		assertArrayEquals(bitmap2, alloc.getAllocationBitmap().getBitmap());
		assertEquals(13, alloc.getPosition());
		assertEquals(10, result.getAddress());
		assertEquals(3, result.getSize());
	}
	
	/**
	 * Teste Allokation bei passender Lücke vor position
	 */
	@Test
	void allocateTest4() {
		boolean[] bitmap = {
				true, false, false, true, true, false, false, true,
				true, true, false, false, true, false, true, true
		};
		NaiveAllocator alloc = new NaiveAllocator(bitmap, 12);
		HeapObject result = alloc.newObject(2);
		
		boolean[] bitmap2 = {
				true, true, true, true, true, false, false, true,
				true, true, false, false, true, false, true, true
		};
		assertArrayEquals(bitmap2, alloc.getAllocationBitmap().getBitmap());
		assertEquals(3, alloc.getPosition());
		assertEquals(1, result.getAddress());
		assertEquals(2, result.getSize());
	}
	
	/**
	 * Teste Allokation bei passender Lücke am Heapende
	 */
	@Test
	void allocateTest5() {
		boolean[] bitmap = {
				true, true, true, true, true, true, true, true,
				true, true, true, true, true, true, false, false
		};
		NaiveAllocator alloc = new NaiveAllocator(bitmap, 3);
		HeapObject result = alloc.newObject(2);
		
		boolean[] bitmap2 = {
				true, true, true, true, true, true, true, true,
				true, true, true, true, true, true, true, true
		};
		assertArrayEquals(bitmap2, alloc.getAllocationBitmap().getBitmap());
		assertEquals(0, alloc.getPosition());
		assertEquals(14, result.getAddress());
		assertEquals(2, result.getSize());
	}
	
	/**
	 * Teste Allokation, wenn keine passende Lücke vorhanden
	 * Beginn am Heapanfang
	 */
	@Test
	void allocateTest6() {
		boolean[] bitmap = {
				false, false, false, true, true, false, false, true,
				true, true, false, false, true, false, true, true
		};
		NaiveAllocator alloc = new NaiveAllocator(bitmap, 0);
		HeapObject result = alloc.newObject(5);
		
		boolean[] bitmap2 = {
				false, false, false, true, true, false, false, true,
				true, true, false, false, true, false, true, true
		};
		assertArrayEquals(bitmap2, alloc.getAllocationBitmap().getBitmap());
		assertEquals(0, alloc.getPosition());
		assertNull(result);
	}
	
	/**
	 * Teste Allokation, wenn keine passende Lücke vorhanden
	 * Beginn mitten im Heap
	 */
	@Test
	void allocateTest7() {
		boolean[] bitmap = {
				false, false, false, true, true, false, false, true,
				true, true, false, false, true, false, true, true
		};
		NaiveAllocator alloc = new NaiveAllocator(bitmap, 7);
		HeapObject result = alloc.newObject(5);
		
		boolean[] bitmap2 = {
				false, false, false, true, true, false, false, true,
				true, true, false, false, true, false, true, true
		};
		assertArrayEquals(bitmap2, alloc.getAllocationBitmap().getBitmap());
		assertEquals(7, alloc.getPosition());
		assertNull(result);
	}
	
	/**
	 * Teste Allokation bei freiem Heap und Startposition hinten
	 */
	@Test
	void allocateTest8() {
		boolean[] bitmap = {
				false, false, false, false, false, false, false, false,
				false, false, false, false, false, false, false, false
		};
		NaiveAllocator alloc = new NaiveAllocator(bitmap, 12);
		HeapObject result = alloc.newObject(6);
		
		boolean[] bitmap2 = {
				true, true, true, true, true, true, false, false,
				false, false, false, false, false, false, false, false
		};
		assertArrayEquals(bitmap2, alloc.getAllocationBitmap().getBitmap());
		assertEquals(6, alloc.getPosition());
		assertEquals(0, result.getAddress());
		assertEquals(6, result.getSize());
	}
	
	/**
	 * Teste Übergabe ungültiger Parameter an free
	 */
	@Test
	void freeException() {
		NaiveAllocator alloc = new NaiveAllocator(new boolean[16], 7);
		assertThrows(IllegalArgumentException.class, () -> alloc.free(null));
		// Teste Übergabe eines zu großen Objekts
		assertThrows(IllegalArgumentException.class, () -> alloc.free(new HeapObject(12, 5)));
	}
	
	/**
	 * Teste Freigabe in der Mitte des Heaps
	 */
	@Test
	void freeTest1() {
		boolean[] bitmap = {
				true, false, false, true, true, true, true, true,
				true, true, false, false, true, false, true, true
		};
		NaiveAllocator alloc = new NaiveAllocator(bitmap, 7);
		alloc.free(new HeapObject(4, 2));
		
		boolean[] bitmap2 = {
				true, false, false, true, false, false, true, true,
				true, true, false, false, true, false, true, true
		};
		assertArrayEquals(bitmap2, alloc.getAllocationBitmap().getBitmap());
		assertEquals(7, alloc.getPosition());       // position soll unverändert bleiben
	}
	
	/**
	 * Teste Freigabe am Anfang des Heaps
	 */
	@Test
	void freeTest2() {
		boolean[] bitmap = {
				true, true, true, true, true, true, true, true,
				true, true, false, false, true, false, true, true
		};
		NaiveAllocator alloc = new NaiveAllocator(bitmap, 2);
		alloc.free(new HeapObject(0, 4));
		
		boolean[] bitmap2 = {
				false, false, false, false, true, true, true, true,
				true, true, false, false, true, false, true, true
		};
		assertArrayEquals(bitmap2, alloc.getAllocationBitmap().getBitmap());
		assertEquals(2, alloc.getPosition());
	}
	
	/**
	 * Teste Freigabe am Ende des Heaps
	 */
	@Test
	void freeTest3() {
		boolean[] bitmap = {
				true, true, true, true, true, true, true, true,
				true, true, false, false, true, true, true, true
		};
		NaiveAllocator alloc = new NaiveAllocator(bitmap, 13);
		alloc.free(new HeapObject(13, 3));
		
		boolean[] bitmap2 = {
				true, true, true, true, true, true, true, true,
				true, true, false, false, true, false, false, false
		};
		assertArrayEquals(bitmap2, alloc.getAllocationBitmap().getBitmap());
		assertEquals(13, alloc.getPosition());
	}
	
	/**
	 * Testet den Umgang von {@code setPosition} mit ungültigen Parametern.
	 */
	@Test
	void setPositionException() {
		NaiveAllocator alloc = new NaiveAllocator(16);
		assertThrows(IllegalArgumentException.class, () -> alloc.setPosition(-3));
		assertThrows(IllegalArgumentException.class, () -> alloc.setPosition(16));
	}
	
	/**
	 * Teste Verschiebung in freien Bereich
	 */
	@Test
	void moveTest1() {
		boolean[] bitmap = {
				false, false, false, true, true, false, false, true,
				true, true, false, false, false, false, true, true
		};
		NaiveAllocator alloc = new NaiveAllocator(bitmap, 2);
		HeapObject obj = new HeapObject(3, 2);
		alloc.moveObject(obj, 1);
		
		boolean[] bitmap2 = {
				false, true, true, false, false, false, false, true,
				true, true, false, false, false, false, true, true
		};
		assertArrayEquals(bitmap2, alloc.getAllocationBitmap().getBitmap());
		assertEquals(2, alloc.getPosition());       // Position soll gleich bleiben
		assertEquals(1, obj.getAddress());          // Objektadresse soll stimmen
	}
	
	/**
	 * Teste Verschiebung in belegten Bereich
	 */
	@Test
	void moveTest2() {
		boolean[] bitmap = {
				false, false, false, true, true, true, true, true,
				true, true, false, false, false, false, true, true
		};
		NaiveAllocator alloc = new NaiveAllocator(bitmap, 7);
		HeapObject obj = new HeapObject(14, 2);
		alloc.moveObject(obj, 4);
		
		boolean[] bitmap2 = {
				false, false, false, true, true, true, true, true,
				true, true, false, false, false, false, false, false
		};
		assertArrayEquals(bitmap2, alloc.getAllocationBitmap().getBitmap());
		assertEquals(7, alloc.getPosition());       // Position soll gleich bleiben
		assertEquals(4, obj.getAddress());          // Objektadresse soll stimmen
	}
	
	/**
	 * Teste Verschiebung bei Überschneidung von Quell- und Zielbereich
	 */
	@Test
	void moveTest3() {
		boolean[] bitmap = {
				false, false, false, true, true, true, true, true,
				true, false, false, false, false, false, true, true
		};
		NaiveAllocator alloc = new NaiveAllocator(bitmap, 11);
		HeapObject obj = new HeapObject(4, 4);
		alloc.moveObject(obj, 6);
		
		boolean[] bitmap2 = {
				false, false, false, true, false, false, true, true,
				true, true, false, false, false, false, true, true
		};
		assertArrayEquals(bitmap2, alloc.getAllocationBitmap().getBitmap());
		assertEquals(11, alloc.getPosition());       // Position soll gleich bleiben
		assertEquals(6, obj.getAddress());          // Objektadresse soll stimmen
	}
	
	/**
	 * Teste Verschiebung bei ungültigen Eingaben.
	 */
	@Test
	void moveExceptions() {
		NaiveAllocator alloc = new NaiveAllocator(16);
		assertThrows(IllegalArgumentException.class, () -> alloc.moveObject(new HeapObject(12, 8), 0));
		assertThrows(IllegalArgumentException.class, () -> alloc.moveObject(new HeapObject(2, 5), -42));
		assertThrows(IllegalArgumentException.class, () -> alloc.moveObject(new HeapObject(2, 5), 13));
	}
	
	/**
	 * Teste clearRange-Methode
	 */
	@Test
	void clearRangeTest() {
		boolean[] bitmap = {
				false, false, true, true, true, true, true, true,
				true, true, false, false, false, false, true, true
		};
		NaiveAllocator alloc = new NaiveAllocator(bitmap, 2);
		alloc.clearRange(3, 7);
		
		boolean[] bitmap2 = {
				false, false, true, false, false, false, false, false,
				true, true, false, false, false, false, true, true
		};
		assertArrayEquals(bitmap2, alloc.getAllocationBitmap().getBitmap());
		assertEquals(2, alloc.getPosition());       // Position soll gleich bleiben
	}
	
	/**
	 * Teste clearRange-Methode mit ungültigen Indizes
	 */
	@Test
	void clearRangeExceptions() {
		NaiveAllocator alloc = new NaiveAllocator(16);
		assertThrows(IllegalArgumentException.class, () -> alloc.clearRange(12, 8));
		assertThrows(IllegalArgumentException.class, () -> alloc.clearRange(-3, 2));
		assertThrows(IllegalArgumentException.class, () -> alloc.clearRange(11, 18));
	}
}