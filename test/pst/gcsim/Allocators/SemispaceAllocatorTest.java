/*
 * Copyright 2019 Phil Steinhorst
 *
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package pst.gcsim.Allocators;
import org.junit.jupiter.api.Test;
import pst.gcsim.Objects.HeapObject;
import static org.junit.jupiter.api.Assertions.*;

/**
 * Testklasse für {@code SemispaceAllocator}.
 */
class SemispaceAllocatorTest {
	
	/**
	 * Teste Übergabe ungültiger Parameter an {@code newObject}.
	 */
	@Test
	void newException() {
		SemispaceAllocator alloc = new SemispaceAllocator(24);
		assertThrows(IllegalArgumentException.class, () -> alloc.newObject(0));
		assertThrows(IllegalArgumentException.class, () -> alloc.newObject(-42));
	}
	
	/**
	 * Teste Allokation in beiden Halbräumen.
	 */
	@Test
	void swapTest() {
		boolean[] bitmap = new boolean[16];
		SemispaceAllocator alloc = new SemispaceAllocator(bitmap, 5, false);
		alloc.swapSemispaces();
		
		assertEquals(8, alloc.getPosition());       // Prüfe neue Position des Allokators
		alloc.setPosition(12);
		alloc.swapSemispaces();
		assertEquals(0, alloc.getPosition());       // Prüfe neue Position des Allokators
	}
	
	/**
	 * Teste Allokation im vorderen Halbraum.
	 */
	@Test
	void allocateTest1() {
		boolean[] bitmap = {
				false, true, true, false, false, false, false, true, true, true, false, false, false, false, false,
				false
		};
		SemispaceAllocator alloc = new SemispaceAllocator(bitmap, 0, false);
		HeapObject result = alloc.newObject(3);
		
		boolean[] bitmap2 = {
				false, true, true, true, true, true, false, true, true, true, false, false, false, false, false, false
		};
		assertArrayEquals(bitmap2, alloc.getAllocationBitmap().getBitmap());        // Prüfe neue Bitmap
		assertEquals(6, alloc.getPosition());       // Prüfe neue Position des Allokators
		assertEquals(3, result.getAddress());       // Prüfe Adresse des erzeugten Objekts
		assertEquals(3, result.getSize());          // Prüfe Größe des neuen Objekts
	}
	
	/**
	 * Teste Allokation im hinteren Halbraum.
	 */
	@Test
	void allocateTest2() {
		boolean[] bitmap = {
				false, true, true, true, true, true, false, true,
				true, true, false, false, false, false, false, false
		};
		SemispaceAllocator alloc = new SemispaceAllocator(bitmap, 8, true);
		HeapObject result = alloc.newObject(5);
		
		boolean[] bitmap2 = {
				false, true, true, true, true, true, false, true,
				true, true, true, true, true, true, true, false
		};
		assertArrayEquals(bitmap2, alloc.getAllocationBitmap().getBitmap());        // Prüfe neue Bitmap
		assertEquals(15, alloc.getPosition());       // Prüfe neue Position des Allokators
		assertEquals(10, result.getAddress());       // Prüfe Adresse des erzeugten Objekts
		assertEquals(5, result.getSize());          // Prüfe Größe des neuen Objekts
	}
	
	/**
	 * Prüfe Beschränkung auf vorderen Halbraum.
	 */
	@Test
	void allocateTest3() {
		boolean[] bitmap = {
				true, true, false, false, false, false, true, false,
				false, false, false, false, false, false, false, false
		};
		SemispaceAllocator alloc = new SemispaceAllocator(bitmap, 4, false);
		HeapObject result = alloc.newObject(3);
		
		boolean[] bitmap2 = {
				true, true, true, true, true, false, true, false,
				false, false, false, false, false, false, false, false
		};
		assertArrayEquals(bitmap2, alloc.getAllocationBitmap().getBitmap());        // Prüfe neue Bitmap
		assertEquals(2, result.getAddress());       // Prüfe Adresse des erzeugten Objekts
		assertEquals(3, result.getSize());          // Prüfe Größe des neuen Objekts
		assertEquals(5, alloc.getPosition());       // Prüfe neue Position des Allokators
	}
	
	/**
	 * Prüfe Beschränkung auf hinteren Halbraum.
	 */
	@Test
	void allocateTest4() {
		boolean[] bitmap = {
				false, false, false, false, false, false, false, false,
				true, false, false, false, false, true, true, false
		};
		SemispaceAllocator alloc = new SemispaceAllocator(bitmap, 11, true);
		HeapObject result = alloc.newObject(3);
		
		boolean[] bitmap2 = {
				false, false, false, false, false, false, false, false,
				true, true, true, true, false, true, true, false
		};
		assertArrayEquals(bitmap2, alloc.getAllocationBitmap().getBitmap());        // Prüfe neue Bitmap
		assertEquals(9, result.getAddress());       // Prüfe Adresse des erzeugten Objekts
		assertEquals(3, result.getSize());          // Prüfe Größe des neuen Objekts
		assertEquals(12, alloc.getPosition());       // Prüfe neue Position des Allokators
	}
	
	/**
	 * Teste fehlschlagende Allokation im vorderen Halbraum.
	 */
	@Test
	void allocateTest5() {
		boolean[] bitmap = {
				true, false, true, false, true, false, true, false,
				false, false, false, false, false, false, false, false
		};
		SemispaceAllocator alloc = new SemispaceAllocator(bitmap, 3, false);
		HeapObject result = alloc.newObject(2);
		
		boolean[] bitmap2 = {
				true, false, true, false, true, false, true, false,
				false, false, false, false, false, false, false, false
		};
		assertArrayEquals(bitmap2, alloc.getAllocationBitmap().getBitmap());
		assertEquals(3, alloc.getPosition());
		assertNull(result);
	}
	
	/**
	 * Teste fehlschlagende Allokation im hinteren Halbraum.
	 */
	@Test
	void allocateTest6() {
		boolean[] bitmap = {
				false, false, false, false, false, false, false, false,
				true, false, true, false, true, false, true, false
		};
		SemispaceAllocator alloc = new SemispaceAllocator(bitmap, 12, true);
		HeapObject result = alloc.newObject(2);
		
		boolean[] bitmap2 = {
				false, false, false, false, false, false, false, false,
				true, false, true, false, true, false, true, false
		};
		assertArrayEquals(bitmap2, alloc.getAllocationBitmap().getBitmap());
		assertEquals(12, alloc.getPosition());
		assertNull(result);
	}
}