/*
 * Copyright 2019 Phil Steinhorst
 *
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package pst.gcsim.Objects;

import java.util.ArrayList;

/**
 * Klasse zur Realisierung von Heapobjekten.
 *
 * @author Phil Steinhorst
 */
public class HeapObject {
	/**
	 * Zähler zur Festlegung der internen ID.
	 */
	private static int count = 0;
	
	/**
	 * Adresse des Objekts im Heap.
	 */
	private int address;
	
	/**
	 * Größe des Objekts (= Anzahl der belegten Wörter).
	 */
	private int size;
	
	/**
	 * Ausgehende Referenzen auf Heapobjekte.
	 */
	private ArrayList<HeapObject> pointers;
	
	/**
	 * Markierungsinformationen.
	 */
	private int mark;
	
	/**
	 * Interne ID.
	 */
	private int id;
	
	/**
	 * Erzeugt ein Heapobjekt anhand von Größe und Speicheradresse.
	 *
	 * @param address Speicheradresse
	 * @param size    Größe
	 */
	public HeapObject(int address, int size) {
		this(address, size, new ArrayList<>(), 0);
	}
	
	/**
	 * Allgemeiner Konstruktor zur Erzeugung eines Heapobjekts.
	 *
	 * @param address  Adresse
	 * @param size     Größe
	 * @param pointers Menge der ausgehenden Referenzen
	 * @param mark     Markierung des Objekts
	 */
	private HeapObject(int address, int size, ArrayList<HeapObject> pointers, int mark) {
		this.address = address;
		this.size = size;
		this.pointers = pointers;
		this.mark = mark;
		this.id = count;
		count++;
	}
	
	/**
	 * Liefert Speicheradresse eines Objekts.
	 *
	 * @return Attribut {@code address}.
	 */
	public int getAddress() {
		return address;
	}
	
	/**
	 * Setzt Speicheradresse des Objekts.
	 *
	 * @param address Neue Speicheradresse.
	 */
	public void setAddress(int address) {
		this.address = address;
	}
	
	/**
	 * Liefert Größe des Objekts.
	 *
	 * @return Attribut {@code size}.
	 */
	public int getSize() {
		return size;
	}
	
	/**
	 * Liefert Menge der ausgehenden Referenzen.
	 *
	 * @return Attribut {@code pointers}.
	 */
	public ArrayList<HeapObject> getPointers() {
		return pointers;
	}
	
	/**
	 * Liefert Markierungsinformation des Objekts.
	 *
	 * @return Attirbut {@code mark}.
	 */
	public int getMark() {
		return mark;
	}
	
	/**
	 * Setzt Markierungsinformation des Objekts.
	 *
	 * @param mark Neue Markierung.
	 */
	public void setMark(int mark) {
		this.mark = mark;
	}
	
	/**
	 * Liefert ID des Objekts.
	 *
	 * @return Attribut {@code id}.
	 */
	public int getId() {
		return id;
	}
}