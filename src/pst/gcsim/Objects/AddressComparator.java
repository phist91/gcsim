/*
 * Copyright 2019 Phil Steinhorst
 *
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package pst.gcsim.Objects;

import java.util.Comparator;

/**
 * Vergleicht zwei Objekte nach ihrer Speicheradresse. Objektmengen können damit sortiert werden, sodass eine lineare
 * Traversierung des Heaps simuliert werden kann. Damit zwei verschiedene Objekte mit gleicher Speicheradresse nicht
 * fälschlicherweise als gleich erkannt werden (das kann etwa beim LISP-2-Algorithmus passieren, wenn Objekte
 * übereinander geschoben werden), werden sie zusätzlich anhand ihrer internen ID verglichen.
 *
 * @author Phil Steinhorst
 */
public class AddressComparator implements Comparator<HeapObject> {
	
	@Override
	public int compare(HeapObject first, HeapObject second) {
		if (first.getAddress() == second.getAddress())
			return (Integer.compare(first.getId(), second.getId()));
		else return Integer.compare(first.getAddress(), second.getAddress());
	}
}