/*
 * Copyright 2019 Phil Steinhorst
 *
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package pst.gcsim.GarbageCollectors;
import pst.gcsim.Allocators.Allocator;
import pst.gcsim.Controllers.CollectionController;

import javax.swing.*;

/**
 * Schnittstelle für einen Garbage Collector.
 *
 * @author Phil Steinhorst
 */
public abstract class GarbageCollector<T extends Allocator> {
	/**
	 * Controller zur Steuerung von Allokator und Kollektor.
	 */
	CollectionController<T> controller;
	
	/**
	 * Animationstimer.
	 */
	Timer gcTimer;
	
	/**
	 * Gibt an, ob die Garbage Collection aktiv ist. (Workaround für Tests der GUI-kompatiblen Methoden)
	 */
	boolean working;
	
	/**
	 * Methode zur Auslösung der Garbage Collection ohne GUI.
	 */
	public void collect() {
		collect(false);
	}
	
	/**
	 * Methode zur Auslösung der Garbage Collection mit Option für GUI-Ausgabe.
	 *
	 * @param timed {@code true}, falls GUI-Ausgabe genutzt werden soll, sonst {@code false}.
	 */
	public abstract void collect(boolean timed);
	
	/**
	 * Liefert Swing-Timer für Animation der GUI-Ausgabe.
	 *
	 * @return Swing-Timer.
	 */
	public Timer getTimer() {
		return gcTimer;
	}
	
	/**
	 * Verknüpft die Garbage Collection mit der Steuerklasse.
	 *
	 * @param controller Steuerklasse
	 */
	public void initialize(CollectionController<T> controller) {
		this.controller = controller;
	}
	
	/**
	 * Gibt an, ob die Garbage Collection einsatzbereit ist.
	 *
	 * @return {@code true}, wenn Verbindung zur Steuerklasse besteht, sonst {@code false}.
	 */
	boolean isInitialized() { return (controller != null); }
	
	/**
	 * Liefert Namen des Garbage Collectors.
	 *
	 * @return Name des Kollektors.
	 */
	public abstract String getName();
	
	/**
	 * Gibt an, ob die Garbage Collection aktiv ist. (Workaround für Tests der GUI-kompatiblen Methoden)
	 *
	 * @return Attribut {@code working}
	 */
	public boolean isWorking() { return working; }
}