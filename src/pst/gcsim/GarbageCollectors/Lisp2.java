/*
 * Copyright 2019 Phil Steinhorst
 *
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package pst.gcsim.GarbageCollectors;

import pst.gcsim.Allocators.Allocator;
import pst.gcsim.Controllers.CollectionController;
import pst.gcsim.Objects.AddressComparator;
import pst.gcsim.Objects.HeapObject;
import pst.gcsim.Settings;

import javax.swing.*;
import java.util.ArrayList;
import java.util.Iterator;

/**
 * Klasse zur Realisierung des Lisp 2 Garbage Collectors.
 *
 * @author Phil Steinhorst
 */
public class Lisp2<T extends Allocator> extends MarkSweep<T> {
	/**
	 * Neue Position, an die ein Objekt verschoben wird.
	 */
	private int newPosition;
	
	/**
	 * Konstruktor zur Erzeugung eines Lisp-2-Garbage-Collectors ohne Komponenten.
	 */
	public Lisp2() {
		this(null);
	}
	
	/**
	 * Konstruktor zur Erzeugung eines Lisp 2 Garbage Collectors.
	 *
	 * @param ctrl Controller, der Allokator und Kollektor steuert.
	 */
	public Lisp2(CollectionController<T> ctrl) {
		super(ctrl);
		newPosition = 0;
	}
	
	/**
	 * Liefert Namen des Garbage Collectors.
	 */
	@Override
	public String getName() {
		return "Lisp 2";
	}
	
	/**
	 * Ausführung der Bereinigungsphase (ohne Animation).
	 */
	@Override
	void sweep() {
		controller.getObjects().sort(new AddressComparator());
		newPosition = 0;
		ArrayList<HeapObject> toRemove = new ArrayList<>();     // Verhinderung von ConcurrentModificationExceptions
		for (HeapObject obj : controller.getObjects())
			if (obj.getMark() == MS_WHITE)
				toRemove.add(obj);
			else {
				controller.getAllocator().moveObject(obj, newPosition);
				newPosition += obj.getSize();
			}
		
		toRemove.forEach(obj -> controller.getObjects().remove(obj));
		
		controller.getAllocator().clearRange(newPosition, controller.getAllocator().getHeapSize() - 1);
		controller.getAllocator().setPosition(newPosition);    // nächste Allokation am Ende des kompaktierten Bereichs
		controller.getObjects().forEach(obj -> obj.setMark(MS_WHITE));
	}
	
	/**
	 * Ausführung der Bereinigungsphase (mit Animation).
	 */
	@Override
	void sweepTimed() {
		controller.getObjects().sort(new AddressComparator());
		controller.setStatus("Starte Bereinigungsphase...", false);
		newPosition = 0;
		ArrayList<HeapObject> heap = new ArrayList<>(
				controller.getObjects());     // Verhinderung von ConcurrentModificationExceptions
		Iterator<HeapObject> iterator = heap.iterator();          // Iteration über Heap
		gcTimer = new Timer(Settings.ANIMATION_SPEED, action -> {
			if (iterator.hasNext()) {
				HeapObject obj = iterator.next();
				if (obj.getMark() == MS_WHITE) {
					controller.getObjects().remove(obj);
				} else {
					controller.getAllocator().moveObject(obj, newPosition);
					newPosition += obj.getSize();
					controller.getAllocator().setPosition(newPosition);
				}
				controller.refreshView();
			} else {
				gcTimer.stop();
				controller.getAllocator().clearRange(newPosition, controller.getAllocator().getHeapSize() - 1);
				controller.getAllocator().setPosition(newPosition);
				controller.getObjects().forEach(obj -> obj.setMark(MS_WHITE));
				controller.setInputs(true);
				controller.setStatus("Kollektion beendet!", false);
				working = false;
				controller.refreshView();
			}
		});
		gcTimer.start();
	}
}