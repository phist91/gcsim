/*
 * Copyright 2019 Phil Steinhorst
 *
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package pst.gcsim.GarbageCollectors;

import pst.gcsim.Allocators.Allocator;
import pst.gcsim.Controllers.CollectionController;
import pst.gcsim.Objects.AddressComparator;
import pst.gcsim.Objects.HeapObject;
import pst.gcsim.Settings;

import javax.swing.Timer;
import java.util.*;

/**
 * Klasse zur Realisierung eines Mark-Sweep-Kollektors.
 *
 * @author Phil Steinhorst
 */
public class MarkSweep<T extends Allocator> extends GarbageCollector<T> {
	/**
	 * Objektmarkierung <i>weiß</i>.
	 */
	public static final int MS_WHITE = 0;
	
	/**
	 * Objektmarkierung <i>grau</i>.
	 */
	public static final int MS_GRAY = 1;
	
	/**
	 * Objektmarkierung <i>schwarz</i>.
	 */
	public static final int MS_BLACK = 2;
	
	/**
	 * Menge der markierten, noch abzuarbeitenden Objekte (FIFO).
	 */
	LinkedList<HeapObject> toScan;
	
	/**
	 * Konstruktor zur Erzeugung eines Mark-Sweep-Garbage-Collectors ohne Komponenten.
	 */
	public MarkSweep() {
		this(null);
	}
	
	/**
	 * Konstruktor zur Erzeugung eines Mark-Sweep-Garbage-Collectors.
	 *
	 * @param controller Controller, der Allokator und Kollektor steuert
	 */
	public MarkSweep(CollectionController<T> controller) {
		this.toScan = new LinkedList<>();
		this.controller = controller;
	}
	
	/**
	 * Methode zur Auslösung der Garbage Collection.
	 *
	 * @param timed {@code true}, falls Animation benötigt wird, sonst {@code false}.
	 */
	public void collect(boolean timed) {
		if (!isInitialized())
			throw new IllegalStateException("Garbage Collection ist nicht initialisiert.");
		if (timed)
			if (controller == null)
				throw new IllegalStateException(
						"CollectionController ist nicht eingerichtet. Keine GUI-Ausgabe möglich.");
			else markStartTimed();
		else {
			markStart();
			sweep();
		}
	}
	
	/**
	 * Liefert Namen des Garbage Collectors.
	 */
	public String getName() {
		return "Mark Sweep";
	}
	
	/**
	 * Startet die Markierungsphase (mit Animation).
	 */
	private void markStartTimed() {
		working = true;
		controller.setStatus("Erfasse Basisobjekte...", false);
		toScan.clear();
		Iterator<HeapObject> iterator = controller.getRoots().iterator();      // zunächst Iteration über roots
		gcTimer = new Timer(Settings.ANIMATION_SPEED, action -> {
			if (iterator.hasNext()) {
				HeapObject obj = iterator.next();     // mit jeder Timer-Iteration ein Basisobjekt abarbeiten
				if (obj.getMark() == MS_WHITE) {
					obj.setMark(MS_GRAY);
					toScan.add(obj);
					controller.refreshView();     // Anzeige aktualisieren
				}
			} else {
				gcTimer.stop();
				markTimed();        // nächste Mark-Phase ausführen
			}
		});
		gcTimer.start();              // starte erzeugten Timer
	}
	
	/**
	 * Startet die Markierungsphase (ohne Animation).
	 */
	void markStart() {
		toScan.clear();
		for (HeapObject obj : controller.getRoots())
			if (obj.getMark() == MS_WHITE) {
				obj.setMark(MS_GRAY);
				toScan.add(obj);
				mark();
			}
	}
	
	/**
	 * Ausführung der Bereinigungsphase (ohne Animation).
	 */
	void sweep() {
		// Objekte nach Adresse sortieren, um lineare Traversierung zu simulieren
		controller.getObjects().sort(new AddressComparator());
		ArrayList<HeapObject> toRemove = new ArrayList<>();     // Verhinderung von ConcurrentModificationExceptions
		for (HeapObject obj : controller.getObjects())
			if (obj.getMark() == MS_WHITE) toRemove.add(obj);
			else obj.setMark(MS_WHITE);
		toRemove.forEach(obj -> {
			controller.getAllocator().free(obj);
			controller.getObjects().remove(obj);
		});
	}
	
	/**
	 * Abarbeitung der toScan-Menge (mit Animation).
	 */
	private void markTimed() {
		controller.setStatus("Abarbeitung entdeckter Objekte...", false);
		gcTimer = new Timer(Settings.ANIMATION_SPEED, action -> {
			if (!toScan.isEmpty()) {                // mit jeder Iteation ein entdecktes Objekt abarbeiten
				HeapObject obj = toScan.remove();
				obj.setMark(MS_BLACK);
				for (HeapObject target : obj.getPointers())
					if (target.getMark() == MS_WHITE) {
						target.setMark(MS_GRAY);
						toScan.add(target);
					}
				controller.refreshView();
			} else {
				gcTimer.stop();
				sweepTimed();          // Sweep-Phase ausführen
			}
		});
		gcTimer.start();
	}
	
	/**
	 * Abarbeitung der toScan-Menge (ohne Animation).
	 */
	private void mark() {
		while (!toScan.isEmpty()) {
			HeapObject obj = toScan.remove();
			obj.setMark(MS_BLACK);
			for (HeapObject target : obj.getPointers())
				if (target.getMark() == MS_WHITE) {
					target.setMark(MS_GRAY);
					toScan.add(target);
				}
		}
	}
	
	/**
	 * Ausführung der Bereinigungsphase (mit Animation).
	 */
	void sweepTimed() {
		// Objekte nach Adresse sortieren, um lineare Traversierung zu simulieren
		controller.getObjects().sort(new AddressComparator());
		controller.setStatus("Starte Bereinigungsphase...", false);
		// Verhinderung von ConcurrentModificationExceptions
		ArrayList<HeapObject> heap = new ArrayList<>(controller.getObjects());
		Iterator<HeapObject> iterator = heap.iterator();          // Iteration über Heap
		gcTimer = new Timer(Settings.ANIMATION_SPEED, action -> {
			if (iterator.hasNext()) {
				HeapObject obj = iterator.next();
				if (obj.getMark() == MS_WHITE) {
					controller.getAllocator().free(obj);
					controller.getObjects().remove(obj);
				} else obj.setMark(MS_WHITE);
				controller.refreshView();
			} else {
				gcTimer.stop();
				working = false;
				controller.setInputs(true);
				controller.setStatus("Kollektion beendet!", false);
			}
		});
		gcTimer.start();
	}
}