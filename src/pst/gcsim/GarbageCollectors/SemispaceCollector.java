/*
 * Copyright 2019 Phil Steinhorst
 *
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package pst.gcsim.GarbageCollectors;

import pst.gcsim.Allocators.SemispaceAllocator;
import pst.gcsim.Controllers.CollectionController;
import pst.gcsim.Objects.HeapObject;
import pst.gcsim.Settings;

import javax.swing.Timer;
import java.util.*;

/**
 * Klasse zur Realisierung des Halbraumverfahrens nach Fenichel, Yochelson und Cheney.
 *
 * @author Phil Steinhorst
 */
public class SemispaceCollector<T extends SemispaceAllocator> extends GarbageCollector<T> {
	/**
	 * Objektmarkierung <i>unentdeckt</i>.
	 */
	public static final int UNDISCOVERED = 0;
	
	/**
	 * Objektmarkierung <i>entdeckt</i>.
	 */
	public static final int DISCOVERED = 11;
	
	/**
	 * Objektmarkierung <i>fertig abgearbeitet</i>.
	 */
	public static final int DONE = 12;
	
	/**
	 * Objektmarkierung <i>bereits kopiert</i>.
	 */
	public static final int COPIED = 13;
	/**
	 * Menge der markierten, noch abzuarbeitenden Objekte (FIFO).
	 */
	LinkedList<HeapObject> toScan;
	/**
	 * Neue Position, an die ein Objekt verschoben wird.
	 */
	private int newPosition;
	
	/**
	 * Konstruktor zur Erzeugung eines Halbraum-Collectors ohne Komponenten.
	 */
	public SemispaceCollector() {
		this(null);
	}
	
	/**
	 * Konstruktor zur Erzeugung eines Halbraum-Collectors.
	 *
	 * @param controller Controller, der Allokator und Kollektor steuert
	 */
	public SemispaceCollector(CollectionController<T> controller) {
		this.toScan = new LinkedList<>();
		this.controller = controller;
	}
	
	/**
	 * Methode zur Auslösung der Garbage Collection.
	 *
	 * @param timed true, falls Animation benötigt wird, sonst false.
	 */
	public void collect(boolean timed) {
		if (!isInitialized())
			throw new IllegalStateException("Garbage Collection ist nicht initialisiert.");
		controller.getAllocator().swapSemispaces();
		newPosition = controller.getAllocator().getTarget();
		toScan.clear();
		if (timed) {
			if (controller == null)
				throw new IllegalStateException(
						"CollectionController ist nicht eingerichtet. Keine GUI-Ausgabe möglich.");
			rootScanTimed();
		} else {
			findObjects();
			cleanUp();
		}
	}
	
	/**
	 * Liefert Namen des Garbage Collectors.
	 */
	public String getName() {
		return "Halbraumverfahren";
	}
	
	/**
	 * Erfassung der Basisobjekte (animiert).
	 */
	private void rootScanTimed() {
		working = true;
		controller.setStatus("Erfasse Basisobjekte...", false);
		Iterator<HeapObject> iterator = controller.getRoots().iterator();      // zunächst Iteration über roots
		gcTimer = new Timer(Settings.ANIMATION_SPEED, action -> {
			if (iterator.hasNext()) {
				HeapObject obj = iterator.next();     // mit jeder Timer-Iteration ein Basisobjekt abarbeiten
				if (obj.getMark() == UNDISCOVERED) {
					int oldAdr = obj.getAddress();
					obj.setMark(DONE);
					controller.getAllocator().moveObject(obj, newPosition);
					newPosition += obj.getSize();
					controller.getAllocator().setPosition(newPosition);
					// Erstelle Objekt, um alte Kopie von obj anzeigen zu lassen
					HeapObject old = controller.getAllocator().newObject(obj.getSize(), oldAdr);
					old.setMark(COPIED);
					controller.getObjects().add(old);
					controller.refreshView();     // Anzeige aktualisieren
					obj.getPointers().forEach(target -> {
						if (target.getMark() == UNDISCOVERED) {
							toScan.add(target);
							target.setMark(DISCOVERED);
						}
					});
				}
			} else {
				gcTimer.stop();
				copyTimed();        // nächste Mark-Phase ausführen
			}
		});
		gcTimer.start();              // starte erzeugten Timer
	}
	
	/**
	 * Entdeckung erreichbarer Objekte.
	 */
	private void findObjects() {
		for (HeapObject obj : controller.getRoots())
			update(obj);
		while (!toScan.isEmpty()) {
			HeapObject obj = toScan.remove();
			for (HeapObject target : obj.getPointers())
				update(target);
			obj.setMark(DONE);
		}
	}
	
	/**
	 * Entfernung verwaister Objekte aus {@code objects} und Freigabe des alten Halbraums.
	 */
	private void cleanUp() {
		ArrayList<HeapObject> toRemove = new ArrayList<>();
		for (HeapObject obj : controller.getObjects()) {
			if (obj.getMark() != DONE)
				toRemove.add(obj);
			else obj.setMark(UNDISCOVERED);
		}
		
		toRemove.forEach(obj -> controller.getObjects().remove(obj));
		controller.getAllocator().clearSource();
	}
	
	/**
	 * Abarbeitung der toScan-Menge (animiert)
	 */
	private void copyTimed() {
		controller.setStatus("Kopiere entdeckte Objekte...", false);
		gcTimer = new Timer(Settings.ANIMATION_SPEED, action -> {
			if (!toScan.isEmpty()) {
				HeapObject obj = toScan.remove();
				int oldAdr = obj.getAddress();
				obj.setMark(DONE);
				controller.getAllocator().moveObject(obj, newPosition);
				newPosition += obj.getSize();
				controller.getAllocator().setPosition(newPosition);
				// Erstelle Objekt, um alte Kopie von obj anzeigen zu lassen
				HeapObject old = controller.getAllocator().newObject(obj.getSize(), oldAdr);
				old.setMark(COPIED);
				controller.getObjects().add(old);
				controller.refreshView();     // Anzeige aktualisieren
				obj.getPointers().forEach(target -> {
					if (target.getMark() == UNDISCOVERED) {
						toScan.add(target);
						target.setMark(DISCOVERED);
					}
				});
			} else { // toScan abgearbeitet
				gcTimer.stop();
				controller.setStatus("Verwerfe Halbraum...", false);
				cleanUp();
				working = false;
				controller.setStatus("Kollektion beendet!", false);
				controller.setInputs(true);
				controller.refreshView();
			}
		});
		gcTimer.start();              // starte erzeugten Timer
	}
	
	/**
	 * Methode zur Verschiebung eines Objekts in den anderen Halbraum.
	 *
	 * @param obj zu verschiebendes Objekt.
	 */
	private void update(HeapObject obj) {
		if (obj.getMark() == UNDISCOVERED) {
			obj.setMark(DISCOVERED);
			controller.getAllocator().moveObject(obj, newPosition);
			newPosition += obj.getSize();
			toScan.add(obj);
		}
	}
}