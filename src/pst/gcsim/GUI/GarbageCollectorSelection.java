/*
 * Copyright 2019 Phil Steinhorst
 *
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package pst.gcsim.GUI;

/**
 * Klasse zur Realisierung von Combobox-Items, um die verschiedenen Kollektoren auswählbar zu machen.
 *
 * @author Phil Steinhorst
 */
public class GarbageCollectorSelection {
	/**
	 * Anzuzeigender Name des Garbage-Kollektors.
	 */
	private String name;
	
	/**
	 * Anweisung, die ausgeführt werden soll, um den Einstelldialog zu öffnen.
	 */
	private Runnable settingsDialogCreate;
	
	/**
	 * Anweisung, die ausgeführt werden soll, um den {@code CollectionController} zu erzeugen.
	 */
	private Runnable collectionControllerCreate;
	
	/**
	 * Konstruktor ohne Argument für den Einstelldialog.
	 *
	 * @param name                       Anzuzeigender Name des Garbage-Kollektors.
	 * @param collectionControllerCreate Anweisung, die ausgeführt werden soll, um den {@code CollectionController} zu
	 *                                   erzeugen.
	 */
	public GarbageCollectorSelection(String name, Runnable collectionControllerCreate) {
		this(name, collectionControllerCreate, null);
	}
	
	/**
	 * Standardkonstruktor mit allen Parametern.
	 *
	 * @param name                       Anzuzeigender Name des Garbage-Kollektors.
	 * @param collectionControllerCreate Anweisung, die ausgeführt werden soll, um den {@code CollectionController} zu
	 * @param settingsDialogCreate       Anweisung, die ausgeführt werden soll, um den Einstelldialog zu öffnen.
	 */
	public GarbageCollectorSelection(String name, Runnable collectionControllerCreate, Runnable settingsDialogCreate) {
		this.name = name;
		this.settingsDialogCreate = settingsDialogCreate;
		this.collectionControllerCreate = collectionControllerCreate;
	}
	
	/**
	 * Liefert Name des Garbage Collectors.
	 */
	public String toString() {
		return name;
	}
	
	/**
	 * Liefert Anweisung zum Öffnen des Einstelldialogs.
	 *
	 * @return Attribut {@code settingsDialogCreate}.
	 */
	public Runnable getSettingsDialogCreate() {
		return settingsDialogCreate;
	}
	
	/**
	 * Liefert Anweisung zur Erzeugung des CollectionControllers.
	 *
	 * @return Attribut {@code collectionControllerCreate}.
	 */
	public Runnable getCollectionControllerCreate() {
		return collectionControllerCreate;
	}
}