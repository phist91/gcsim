/*
 * Copyright 2019 Phil Steinhorst
 *
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package pst.gcsim.GUI;

import pst.gcsim.GarbageCollectors.MarkSweep;
import pst.gcsim.GarbageCollectors.SemispaceCollector;
import pst.gcsim.Objects.HeapObject;
import pst.gcsim.Settings;

import javax.swing.*;
import java.awt.*;
import java.util.List;

/**
 * Formular mit Zeichenfläche zur Darstellung des Heaps.
 *
 * @author Phil Steinhorst
 */
public class CanvasView extends JFrame {
	/**
	 * Menge der zu zeichnenden Objekte.
	 */
	private List<HeapObject> objects;
	
	/**
	 * aktuelle Allokatorposition.
	 */
	private int positionMarker = 0;
	
	/**
	 * Erzeugt einen neuen Frame mit einem Canvas zum Zeichnen des Heaps.
	 *
	 * @param objects Menge der zu zeichnenden Heapobjekte.
	 */
	public CanvasView(List<HeapObject> objects) {
		super("Heap");
		this.objects = objects;
		setDefaultCloseOperation(JFrame.HIDE_ON_CLOSE);
		setResizable(false);
		add(new CanvasPanel());
		pack();
		setLocation(600, 50);
		setVisible(true);
	}
	
	/**
	 * Methode zur Aktualisierung der Grafik. Wird nach einer Manipulation des Heaps aufgerufen.
	 *
	 * @param positionMarker Aktuelle Position des Allokators.
	 */
	public void refreshFrame(int positionMarker) {
		this.positionMarker = positionMarker;
		this.revalidate();
		this.repaint();
	}
	
	/**
	 * Interne Klasse mit Operationen zum Zeichnen des Canvas.
	 */
	private class CanvasPanel extends JPanel {
		/**
		 * Erzeugt Zeichenfläche
		 */
		CanvasPanel() {
			setBorder(BorderFactory.createLineBorder(Color.black));
		}
		
		/**
		 * Wird bei Neuzeichnung des Frames ausgeführt.
		 *
		 * @param g Zeichenflöche
		 */
		@Override
		public void paintComponent(Graphics g) {
			super.paintComponent(g);
			g.setColor(Color.WHITE);
			g.fillRect(0, 0, Settings.canvasWidth(), Settings.canvasHeight());
			drawGrid(g);
			// Zeichne jedes Objekt...
			for (HeapObject obj : objects)
				drawObject(g, obj);
			drawPositionMarker(g);
		}
		
		/**
		 * Bevorzugte Größe der Zeichenfläche.
		 *
		 * @return Größe der Zeichenfläche.
		 */
		@Override
		public Dimension getPreferredSize() {
			return new Dimension(Settings.canvasWidth(), Settings.canvasHeight());
		}
		
		/**
		 * Zeichnet das Grundraster.
		 *
		 * @param g Zeichenfläche
		 */
		private void drawGrid(Graphics g) {
			// horizontale Linien
			g.setColor(Color.GRAY);
			for (int i = 0; i <= Settings.HEAP_ROWS; i++) {
				g.drawLine(Settings.MARGIN_HORIZ, Settings.CELL_HEIGHT * i + Settings.MARGIN_VERT,
				           Settings.canvasWidth() - Settings.MARGIN_HORIZ,
				           Settings.CELL_HEIGHT * i + Settings.MARGIN_VERT);
			}
			
			// vertikale Linien
			for (int i = 0; i <= Settings.HEAP_COLS; i++) {
				g.drawLine(Settings.CELL_WIDTH * i + Settings.MARGIN_HORIZ, Settings.MARGIN_VERT,
				           Settings.CELL_WIDTH * i + Settings.MARGIN_HORIZ,
				           Settings.canvasHeight() - Settings.MARGIN_VERT);
			}
		}
		
		/**
		 * Methode zum Zeichnen eines einzelnen Objekts.
		 *
		 * @param g   Zeichenfläche
		 * @param obj Zu zeichnendes Objekt
		 */
		private void drawObject(Graphics g, HeapObject obj) {
			// Bestimme Startzelle
			int row = obj.getAddress() / Settings.HEAP_COLS;
			int col = obj.getAddress() % Settings.HEAP_COLS;
			int remaining = obj.getSize();          // noch zu zeichnende Zellen
			Color color;
			switch (obj.getMark()) {                 // Farbe anhand Markierung fetlegen
				case MarkSweep.MS_WHITE:
				case SemispaceCollector.DISCOVERED:
					color = Settings.COLOR_DEAD;
					break;
				case MarkSweep.MS_GRAY:
					color = Settings.COLOR_MARKED;
					break;
				case MarkSweep.MS_BLACK:
					color = Settings.COLOR_LIVE;
					break;
				case SemispaceCollector.COPIED:
					color = Settings.COLOR_COPIED;
					break;
				case SemispaceCollector.DONE:
					color = Settings.COLOR_LIVE;
					break;
				default:
					color = Color.WHITE;
			}
			
			// Zeichne Objekt Zeile für Zeile
			while (remaining > Settings.HEAP_COLS - col) {
				drawBlock(g, col, row, Settings.HEAP_COLS - col, color);
				remaining = remaining - (Settings.HEAP_COLS - col);
				++row;
				col = 0;
				
			}
			
			if (remaining > 0) drawBlock(g, col, row, remaining, color);
			
			// Markiere Objektbeginn
			drawObjectStart(g, obj.getAddress() % Settings.HEAP_COLS, obj.getAddress() / Settings.HEAP_COLS);
			drawObjectEnd(g, col + remaining, row);
		}
		
		/**
		 * Zeichnet aktuelle Position des Allokators.
		 *
		 * @param g Zeichenfläche
		 */
		private void drawPositionMarker(Graphics g) {
			int xpos = Settings.CELL_WIDTH * (positionMarker % Settings.HEAP_COLS) + Settings.MARGIN_HORIZ;
			int ypos = Settings.CELL_HEIGHT * (positionMarker / Settings.HEAP_COLS) + Settings.MARGIN_VERT;
			g.setColor(Color.RED);
			g.fillRect(xpos, ypos - 3, 2, Settings.CELL_HEIGHT + 6);
		}
		
		/**
		 * Zeichnet ein Rechteck in Abhängigkeit von Objektposition, -markierung und -größe.
		 *
		 * @param g      Zeichenfläche
		 * @param x      Beginn des Rechtecks (Spaltenindex)
		 * @param y      Beginn des Rechtecks (Zeilenindex)
		 * @param blocks Anzahl der zu überdeckenden Zellen
		 * @param color  Farbe des Rechtecks
		 */
		private void drawBlock(Graphics g, int x, int y, int blocks, Color color) {
			int xpos = Settings.CELL_WIDTH * x + Settings.MARGIN_HORIZ;
			int ypos = Settings.CELL_HEIGHT * y + Settings.MARGIN_VERT;
			int width = Settings.CELL_WIDTH * blocks;
			g.setColor(color);
			g.fillRect(xpos + 1, ypos + 1, width - 1, Settings.CELL_HEIGHT - 2);
			g.setColor(Color.BLACK);
			g.fillRect(xpos + 1, ypos + 1, width - 1, 1);
			g.fillRect(xpos + 1, ypos + Settings.CELL_HEIGHT - 1, width, 1);
		}
		
		/**
		 * Zeichnet den Beginn eines Objekts.
		 *
		 * @param g Zeichenfläche
		 * @param x Objektbeginn (Spaltenindex)
		 * @param y Objektbeginn (Zeilenindex)
		 */
		private void drawObjectStart(Graphics g, int x, int y) {
			int xpos = Settings.CELL_WIDTH * x + Settings.MARGIN_HORIZ;
			int ypos = Settings.CELL_HEIGHT * y + Settings.MARGIN_VERT;
			g.setColor(Color.BLACK);
			g.fillRect(xpos + 1, ypos + 1, 1, Settings.CELL_HEIGHT - 1);
		}
		
		/**
		 * Zeichnet den Beginn eines Objekts.
		 *
		 * @param g Zeichenfläche
		 * @param x Objektbeginn (Spaltenindex)
		 * @param y Objektbeginn (Zeilenindex)
		 */
		private void drawObjectEnd(Graphics g, int x, int y) {
			int xpos = Settings.CELL_WIDTH * x + Settings.MARGIN_HORIZ;
			int ypos = Settings.CELL_HEIGHT * y + Settings.MARGIN_VERT;
			g.setColor(Color.BLACK);
			g.fillRect(xpos - 2, ypos + 1, 1, Settings.CELL_HEIGHT - 2);
		}
	}
}