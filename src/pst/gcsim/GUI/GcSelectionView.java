/*
 * Copyright 2019 Phil Steinhorst
 *
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package pst.gcsim.GUI;

import com.intellij.uiDesigner.core.*;
import pst.gcsim.Launcher;
import pst.gcsim.Settings;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;

import static com.intellij.uiDesigner.core.GridConstraints.*;

/**
 * Formular zur Auswahl eines Garbage Collectors.
 *
 * @author Phil Steinhorst
 */
public class GcSelectionView extends JFrame {
	/**
	 * Garbage-Collection-Auswahlfeld.
	 */
	private JComboBox<GarbageCollectorSelection> cbCollector;
	
	/**
	 * Schaltfläche für zusätzliche Einstellungen.
	 */
	private JButton btSettings;
	
	/**
	 * Eingabefelder.
	 */
	private JTextField edHeapWidth, edHeapHeight, edCellWidth, edCellHeight;
	
	/**
	 * Anzeige für gesamte Heapgröße.
	 */
	private JLabel lbHeapSizeEquals;
	
	/**
	 * Anzeige für Gesamtgröße der Zeichenfläche.
	 */
	private JLabel lbCanvasSizeEquals;
	
	/**
	 * Aktuell ausgewählte Garbage Collection.
	 */
	private GarbageCollectorSelection selection;
	
	/**
	 * Konstruktor zur Erzeugung des Fensters.
	 */
	public GcSelectionView() {
		super("Garbage-Collection-Simulator");
		buildView();
	}
	
	/**
	 * Methode zum Aufbau des Fensters.
	 */
	private void buildView() {
		JLabel lbHeapSize = new JLabel();
		lbHeapSize.setText("Heapgröße:");
		lbHeapSize.setToolTipText("Größe des Heaps in Wörtern (Breite x Höhe).");
		JLabel lbX = new JLabel();
		lbX.setText("x");
		JLabel lbX2 = new JLabel();
		lbX2.setText("x");
		lbHeapSizeEquals = new JLabel();
		lbHeapSizeEquals.setText("= " + Settings.heapSize() + " Blöcke");
		lbHeapSizeEquals.setToolTipText("Gesamtgröße des Heaps in Blöcken.");
		JLabel lbCellSize = new JLabel();
		lbCellSize.setText("Zellengröße:");
		lbCellSize.setToolTipText("Größe einer einzelnen Zelle in Pixel (Breite x Höhe).");
		lbCanvasSizeEquals = new JLabel();
		lbCanvasSizeEquals.setText("= " + Settings.HEAP_COLS * Settings.CELL_WIDTH + "x" +
				                           Settings.HEAP_ROWS * Settings.CELL_HEIGHT + " " + "Pixel");
		lbCanvasSizeEquals.setToolTipText("Gesamtgröße der Zeichenfläche.");
		
		cbCollector = new JComboBox<GarbageCollectorSelection>();
		cbCollector.setToolTipText("Zu verwendender Garbage-Collection-Algorithmus.");
		cbCollector.setModel(new DefaultComboBoxModel<GarbageCollectorSelection>(Settings.collectors));
		cbCollector.setSelectedIndex(Settings.LAST_SELECTION);
		selection = cbCollector.getItemAt(Settings.LAST_SELECTION);
		
		edHeapWidth = new JTextField(3);
		edHeapWidth.setText("" + Settings.HEAP_COLS);
		edHeapWidth.setToolTipText("Größe des Heaps in Wörtern (Breite x Höhe).");
		edHeapHeight = new JTextField(3);
		edHeapHeight.setText("" + Settings.HEAP_ROWS);
		edHeapHeight.setToolTipText("Größe des Heaps in Wörtern (Breite x Höhe).");
		edCellWidth = new JTextField(2);
		edCellWidth.setText("" + Settings.CELL_WIDTH);
		edCellWidth.setToolTipText("Größe einer einzelnen Zelle in Pixel (Breite x Höhe).");
		edCellHeight = new JTextField(2);
		edCellHeight.setText("" + Settings.CELL_HEIGHT);
		edCellHeight.setToolTipText("Größe einer einzelnen Zelle in Pixel (Breite x Höhe).");
		
		btSettings = new JButton();
		btSettings.setText("Einstellungen");
		if (selection.getSettingsDialogCreate() != null) {
			btSettings.setEnabled(true);
			btSettings.setToolTipText("Weitere Einstellungen des Kollektors.");
		} else {
			btSettings.setEnabled(false);
			btSettings.setToolTipText("Weitere Einstellungen nicht verfügbar.");
		}
		JButton btOk = new JButton();
		btOk.setText("OK");
		btOk.setToolTipText("Startet den Simulator.");
		JButton btEnd = new JButton();
		btEnd.setText("Beenden");
		btEnd.setToolTipText("Beendet das Programm.");
		JButton btAbout = new JButton();
		btAbout.setText("Über");
		btAbout.setToolTipText("Weitere Informationen zum Programm.");
		
		JPanel gcSelectorPanel = new JPanel();
		gcSelectorPanel.setLayout(new GridLayoutManager(1, 3, new Insets(5, 5, 5, 5), -1, -1));
		gcSelectorPanel.setBorder(BorderFactory.createTitledBorder(BorderFactory.createEtchedBorder(), "Garbage " +
				"Collection"));
		gcSelectorPanel.add(cbCollector, new GridConstraints(0, 0, 1, 1, ANCHOR_WEST, FILL_HORIZONTAL,
		                                                     SIZEPOLICY_CAN_GROW, SIZEPOLICY_FIXED, null,
		                                                     new Dimension(300, -1), null, 0, false));
		gcSelectorPanel.add(new Spacer(), new GridConstraints(0, 1, 1, 1, ANCHOR_CENTER, FILL_HORIZONTAL,
		                                                      SIZEPOLICY_WANT_GROW, 1, null, null, null, 0, false));
		gcSelectorPanel.add(btSettings, new GridConstraints(0, 2, 1, 1, ANCHOR_CENTER, FILL_HORIZONTAL,
		                                                    SIZEPOLICY_CAN_SHRINK | SIZEPOLICY_CAN_GROW,
		                                                    SIZEPOLICY_FIXED, null, null, null, 0, false));
		
		JPanel generalPanel = new JPanel();
		generalPanel.setBorder(BorderFactory.createTitledBorder(BorderFactory.createEtchedBorder(), "Einstellungen"));
		generalPanel.setLayout(new GridLayoutManager(2, 5, new Insets(5, 5, 5, 5), -1, -1));
		generalPanel.add(lbHeapSize, new GridConstraints(0, 0, 1, 1, ANCHOR_WEST, FILL_NONE, SIZEPOLICY_FIXED,
		                                                 SIZEPOLICY_FIXED, null, null, null, 0, false));
		generalPanel.add(lbX, new GridConstraints(0, 2, 1, 1, ANCHOR_CENTER, FILL_NONE, SIZEPOLICY_FIXED,
		                                          SIZEPOLICY_FIXED, null, null, null, 0, false));
		generalPanel.add(lbX2, new GridConstraints(1, 2, 1, 1, ANCHOR_CENTER, FILL_NONE, SIZEPOLICY_FIXED,
		                                           SIZEPOLICY_FIXED, null, null, null, 0, false));
		generalPanel.add(lbHeapSizeEquals, new GridConstraints(0, 4, 1, 1, ANCHOR_WEST, FILL_NONE, SIZEPOLICY_FIXED,
		                                                       SIZEPOLICY_FIXED, new Dimension(150, -1), null,
		                                                       new Dimension(150, -1), 0, false));
		generalPanel.add(lbCellSize, new GridConstraints(1, 0, 1, 1, ANCHOR_WEST, FILL_NONE, SIZEPOLICY_FIXED,
		                                                 SIZEPOLICY_FIXED, null, null, null, 0, false));
		generalPanel.add(lbCanvasSizeEquals, new GridConstraints(1, 4, 1, 1, ANCHOR_WEST, FILL_NONE, SIZEPOLICY_FIXED,
		                                                         SIZEPOLICY_FIXED, new Dimension(150, -1), null,
		                                                         new Dimension(150, -1), 0, false));
		generalPanel.add(edHeapWidth, new GridConstraints(0, 1, 1, 1, ANCHOR_WEST, FILL_HORIZONTAL,
		                                                  SIZEPOLICY_WANT_GROW, SIZEPOLICY_FIXED, null,
		                                                  new Dimension(75, -1), null, 0, false));
		generalPanel.add(edHeapHeight, new GridConstraints(0, 3, 1, 1, ANCHOR_WEST, FILL_HORIZONTAL,
		                                                   SIZEPOLICY_WANT_GROW, SIZEPOLICY_FIXED, null,
		                                                   new Dimension(75, -1), null, 0, false));
		generalPanel.add(edCellWidth, new GridConstraints(1, 1, 1, 1, ANCHOR_WEST, FILL_HORIZONTAL,
		                                                  SIZEPOLICY_WANT_GROW, SIZEPOLICY_FIXED, null,
		                                                  new Dimension(75, -1), null, 0, false));
		generalPanel.add(edCellHeight, new GridConstraints(1, 3, 1, 1, ANCHOR_WEST, FILL_HORIZONTAL,
		                                                   SIZEPOLICY_WANT_GROW, SIZEPOLICY_FIXED, null,
		                                                   new Dimension(75, -1), null, 0, false));
		
		JPanel buttonPanel = new JPanel();
		buttonPanel.setLayout(new GridLayoutManager(1, 4, new Insets(5, 5, 5, 5), -1, -1));
		buttonPanel.add(btOk, new GridConstraints(0, 3, 1, 1, ANCHOR_CENTER, FILL_HORIZONTAL,
		                                          SIZEPOLICY_CAN_SHRINK | SIZEPOLICY_CAN_GROW,
		                                          SIZEPOLICY_FIXED, null, null, null, 0, false));
		buttonPanel.add(new Spacer(), new GridConstraints(0, 1, 1, 1, ANCHOR_CENTER, FILL_HORIZONTAL,
		                                                  SIZEPOLICY_WANT_GROW, 1, null, null, null, 0, false));
		buttonPanel.add(btEnd, new GridConstraints(0, 2, 1, 1, ANCHOR_CENTER, FILL_HORIZONTAL,
		                                           SIZEPOLICY_CAN_SHRINK | SIZEPOLICY_CAN_GROW,
		                                           SIZEPOLICY_FIXED, null, null, null, 0, false));
		buttonPanel.add(btAbout, new GridConstraints(0, 0, 1, 1, ANCHOR_CENTER, FILL_HORIZONTAL,
		                                             SIZEPOLICY_CAN_SHRINK | SIZEPOLICY_CAN_GROW,
		                                             SIZEPOLICY_FIXED, null, null, null, 0, false));
		
		JPanel mainPanel = new JPanel();
		mainPanel.setLayout(new GridLayoutManager(3, 1, new Insets(5, 5, 5, 5), -1, -1));
		mainPanel.add(gcSelectorPanel, new GridConstraints(0, 0, 1, 1, ANCHOR_CENTER, FILL_BOTH,
		                                                   SIZEPOLICY_CAN_SHRINK | SIZEPOLICY_CAN_GROW,
		                                                   SIZEPOLICY_CAN_SHRINK | SIZEPOLICY_CAN_GROW, null, null,
		                                                   null, 0, false));
		mainPanel.add(generalPanel, new GridConstraints(1, 0, 1, 1, ANCHOR_CENTER, FILL_BOTH,
		                                                SIZEPOLICY_CAN_SHRINK | SIZEPOLICY_CAN_GROW,
		                                                SIZEPOLICY_CAN_SHRINK | SIZEPOLICY_CAN_GROW, null, null, null,
		                                                0, false));
		mainPanel.add(buttonPanel, new GridConstraints(2, 0, 1, 1, ANCHOR_CENTER, FILL_BOTH,
		                                               SIZEPOLICY_CAN_SHRINK | SIZEPOLICY_CAN_GROW,
		                                               SIZEPOLICY_CAN_SHRINK | SIZEPOLICY_CAN_GROW, null, null, null,
		                                               0, false));
		
		cbCollector.addActionListener(new ComboBoxSelection());
		edCellHeight.addKeyListener(new TextFieldInput());
		edCellWidth.addKeyListener(new TextFieldInput());
		edHeapHeight.addKeyListener(new TextFieldInput());
		edHeapWidth.addKeyListener(new TextFieldInput());
		btSettings.addActionListener(new SettingsClick());
		btOk.addActionListener(new OkClick(this));
		btEnd.addActionListener(new EndClick());
		btAbout.addActionListener(new AboutClick());
		
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setResizable(false);
		add(mainPanel);
		pack();
		setLocationRelativeTo(null);
		setVisible(true);
		addWindowListener(new WindowClose());
	}
	
	/**
	 * Testet Formulareingaben auf Plausibilität.
	 *
	 * @return {@code true}, wenn Eingaben gültig, sonst {@code false}.
	 */
	private boolean applyInputs() {
		int heapWidth, heapHeight, cellWidth, cellHeight;
		// Teste Formate der Eingaben
		try {
			heapWidth = Integer.parseInt(edHeapWidth.getText());
		} catch (NumberFormatException e) {
			String msg = "Ungültiges Zahlformat für Heapgröße: " + edHeapWidth.getText();
			Launcher.log(msg, true);
			JOptionPane.showMessageDialog(this, msg, "Fehler", JOptionPane.ERROR_MESSAGE);
			return false;
		}
		
		try {
			heapHeight = Integer.parseInt(edHeapHeight.getText());
		} catch (NumberFormatException e) {
			String msg = "Ungültiges Zahlformat für Heapgröße: " + edHeapHeight.getText();
			Launcher.log(msg, true);
			JOptionPane.showMessageDialog(this, msg, "Fehler", JOptionPane.ERROR_MESSAGE);
			return false;
		}
		
		try {
			cellWidth = Integer.parseInt(edCellWidth.getText());
		} catch (NumberFormatException e) {
			String msg = "Ungültiges Zahlformat für Zellengröße: " + edCellWidth.getText();
			Launcher.log(msg, true);
			JOptionPane.showMessageDialog(this, msg, "Fehler", JOptionPane.ERROR_MESSAGE);
			return false;
		}
		
		try {
			cellHeight = Integer.parseInt(edCellHeight.getText());
		} catch (NumberFormatException e) {
			String msg = "Ungültiges Zahlformat für Zellengröße: " + edCellHeight.getText();
			Launcher.log(msg, true);
			JOptionPane.showMessageDialog(this, msg, "Fehler", JOptionPane.ERROR_MESSAGE);
			return false;
		}
		
		// Teste auf sinnvoll gewählte Heapgröße
		if (heapWidth < 10 || heapHeight < 10 || heapHeight > 200 || heapWidth > 200) {
			String msg = "Die Dimensionen des Heaps müssen jeweils zwischen 10 und 200 liegen.";
			Launcher.log(msg, true);
			JOptionPane.showMessageDialog(this, msg, "Fehler", JOptionPane.ERROR_MESSAGE);
			return false;
		}
		
		// Teste auf sinnvoll gewählte Zellengröße
		if (cellWidth < 5 || cellHeight < 5 || cellWidth > 50 || cellHeight > 50) {
			String msg = "Die Dimensionen einer Zelle sollten jeweils zwischen 5 und 50 liegen" + ".";
			Launcher.log(msg, true);
			JOptionPane.showMessageDialog(this, msg, "Fehler", JOptionPane.ERROR_MESSAGE);
			return false;
		}
		
		// übernehme Konfiguration
		Settings.HEAP_COLS = heapWidth;
		Settings.HEAP_ROWS = heapHeight;
		Settings.CELL_WIDTH = cellWidth;
		Settings.CELL_HEIGHT = cellHeight;
		Settings.LAST_SELECTION = cbCollector.getSelectedIndex();
		return true;
	}
	
	private void saveSettings() {
		int ans = JOptionPane.showOptionDialog(null, "Der Simulator wird beendet. Einstellungen speichern?",
		                                       "Simulator beenden", JOptionPane.YES_NO_OPTION,
		                                       JOptionPane.QUESTION_MESSAGE, null, new String[]{"Ja", "Nein"}, null);
		if (ans == 0) {
			String saveResult = Settings.writeSettings();
			if (saveResult != null)
				JOptionPane.showMessageDialog(null, saveResult, "Fehler", JOptionPane.ERROR_MESSAGE);
		}
	}
	
	/**
	 * Interne Klasse zur Behandlung der GC-Auswahl (Anpassung der Einstellungen-Schaltfläche).
	 */
	private class ComboBoxSelection implements ActionListener {
		public void actionPerformed(ActionEvent ae) {
			selection = cbCollector.getItemAt(cbCollector.getSelectedIndex());
			if (selection.getSettingsDialogCreate() != null) {
				btSettings.setEnabled(true);
				btSettings.setToolTipText("Weitere Einstellungen des Kollektors.");
			} else {
				btSettings.setEnabled(false);
				btSettings.setToolTipText("Weitere Einstellungen nicht verfügbar.");
			}
		}
	}
	
	/**
	 * Interne Klasse zur Behandlung von Button-Klicks (Erzeugung des CollectionControllers).
	 */
	private class OkClick implements ActionListener {
		JFrame frame;
		
		OkClick(JFrame frame) {
			this.frame = frame;
		}
		
		public void actionPerformed(ActionEvent ae) {
			if (!applyInputs()) return;
			Launcher.log("Starte Simulator: " + selection.toString(), false);
			selection.getCollectionControllerCreate().run();
			frame.dispose();
		}
	}
	
	/**
	 * Interne Klasse zur Behandlung von Button-Klicks (Einstellungen öffnen).
	 */
	private class SettingsClick implements ActionListener {
		public void actionPerformed(ActionEvent ae) {
			if (selection.getSettingsDialogCreate() != null) selection.getSettingsDialogCreate().run();
		}
	}
	
	/**
	 * Interne Klasse zur Behandlung von Button-Klicks (beenden).
	 */
	private class EndClick implements ActionListener {
		public void actionPerformed(ActionEvent ae) {
			saveSettings();
			Launcher.log("Programm wird beendet...", false);
			System.exit(0);
		}
	}
	
	/**
	 * Interne Klasse zur Behandlung von Button-Klicks (Info-Fenster öffnen).
	 */
	private class AboutClick implements ActionListener {
		public void actionPerformed(ActionEvent e) {
			new AboutView();
		}
	}
	
	/**
	 * Interne Klasse zur Behandlung von Texteingaben (Resultate berechnen).
	 */
	private class TextFieldInput implements KeyListener {
		public void keyTyped(KeyEvent ae) {
			int heapWidth, heapHeight, cellWidth, cellHeight;
			if (edHeapWidth.getText().length() > 3) edHeapWidth.setText(edHeapWidth.getText().substring(0, 3));
			if (edHeapHeight.getText().length() > 3) edHeapHeight.setText(edHeapHeight.getText().substring(0, 3));
			if (edCellHeight.getText().length() > 2) edCellHeight.setText(edCellHeight.getText().substring(0, 2));
			if (edCellWidth.getText().length() > 2) edCellWidth.setText(edCellWidth.getText().substring(0, 2));
			
			try {
				heapWidth = Integer.parseInt(edHeapWidth.getText());
				heapHeight = Integer.parseInt(edHeapHeight.getText());
				cellWidth = Integer.parseInt(edCellWidth.getText());
				cellHeight = Integer.parseInt(edCellHeight.getText());
				lbHeapSizeEquals.setText("= " + heapWidth * heapHeight + " Blöcke");
				lbCanvasSizeEquals.setText("= " + heapWidth * cellWidth + "x" + heapHeight * cellHeight + " Pixel");
			} catch (NumberFormatException e) {
				lbHeapSizeEquals.setText("");
				lbCanvasSizeEquals.setText("");
			}
		}
		
		public void keyPressed(KeyEvent ae) {keyTyped(ae);}
		
		public void keyReleased(KeyEvent ae) {keyTyped(ae);}
	}
	
	/**
	 * Interne Klasse zur Behandlung des Fensterschließens (Einstellungen speichern).
	 */
	private class WindowClose extends WindowAdapter {
		public void windowClosing(WindowEvent we) {
			saveSettings();
			Launcher.log("Programm wird beendet...", false);
		}
	}
}