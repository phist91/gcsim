/*
 * Copyright 2019 Phil Steinhorst
 *
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package pst.gcsim.GUI;

import com.intellij.uiDesigner.core.*;
import pst.gcsim.Settings;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.net.URI;

import static com.intellij.uiDesigner.core.GridConstraints.*;

/**
 * Dialog mit Informationen zum Programm.
 *
 * @author Phil Steinhorst
 */
public class AboutView extends JDialog {
	
	/**
	 * Konstruktor zur Erzeugung des Fensters.
	 */
	public AboutView() {
		buildView();
	}
	
	/**
	 * Methode zum Aufbau des Fensters.
	 */
	private void buildView() {
		JLabel lbTitle = new JLabel();
		lbTitle.setFont(lbTitle.getFont().deriveFont(Font.BOLD).deriveFont(24f));
		lbTitle.setText("Garbage-Collection-Simulator");
		
		JLabel lbVersion = new JLabel();
		lbVersion.setFont(lbVersion.getFont().deriveFont(Font.BOLD).deriveFont(16f));
		lbVersion.setText("Version 1.0.1");
		
		JLabel lbMail = new JLabel();
		lbMail.setFont(new Font("Courier New", Font.PLAIN, 12));
		lbMail.setText(Settings.DEV_MAIL);
		
		JLabel lbAuthor = new JLabel();
		lbAuthor.setText("© 2019 " + Settings.DEV_NAME);
		
		JLabel lbLicense = new JLabel();
		lbLicense.setText("Licensed under the Apache License, Version 2.0");
		
		JLabel lbUrl = new JLabel();
		lbUrl.setFont(new Font("Courier New", Font.PLAIN, 12));
		lbUrl.setText(Settings.REPOSITORY_URL);
		
		JPanel infoPanel = new JPanel();
		infoPanel.setLayout(new GridLayoutManager(7, 1, new Insets(20, 20, 20, 20), -1, -1));
		infoPanel.setBorder(BorderFactory.createTitledBorder(BorderFactory.createEtchedBorder(), null));
		infoPanel.add(lbTitle, new GridConstraints(0, 0, 1, 1, ANCHOR_WEST, FILL_NONE, SIZEPOLICY_FIXED,
		                                           SIZEPOLICY_FIXED, null, null, null, 0, false));
		infoPanel.add(lbVersion, new GridConstraints(1, 0, 1, 1, ANCHOR_EAST, FILL_NONE, SIZEPOLICY_FIXED,
		                                             SIZEPOLICY_FIXED, null, null, null, 0, false));
		infoPanel.add(lbMail, new GridConstraints(4, 0, 1, 1, ANCHOR_WEST, FILL_NONE, SIZEPOLICY_FIXED,
		                                          SIZEPOLICY_FIXED, null, null, null, 2, false));
		infoPanel.add(lbAuthor, new GridConstraints(3, 0, 1, 1, ANCHOR_WEST, FILL_NONE, SIZEPOLICY_FIXED,
		                                            SIZEPOLICY_FIXED, null, null, null, 2, false));
		infoPanel.add(lbLicense, new GridConstraints(5, 0, 1, 1, ANCHOR_WEST, FILL_NONE, SIZEPOLICY_FIXED,
				SIZEPOLICY_FIXED, null, null, null, 2, false));
		infoPanel.add(lbUrl, new GridConstraints(6, 0, 1, 1, ANCHOR_WEST, FILL_NONE, SIZEPOLICY_FIXED,
		                                         SIZEPOLICY_FIXED, null, null, null, 2, false));
		infoPanel.add(new Spacer(), new GridConstraints(2, 0, 1, 1, ANCHOR_CENTER, FILL_VERTICAL, 1,
		                                                SIZEPOLICY_WANT_GROW, null, new Dimension(-1, 30), null, 0,
		                                                false));
		
		JButton btOk = new JButton();
		btOk.setText("OK");
		
		JPanel buttonPanel = new JPanel();
		buttonPanel.setLayout(new GridLayoutManager(1, 2, new Insets(0, 0, 0, 0), -1, -1));
		buttonPanel.add(btOk, new GridConstraints(0, 1, 1, 1, ANCHOR_CENTER, FILL_HORIZONTAL,
		                                          SIZEPOLICY_CAN_SHRINK | SIZEPOLICY_CAN_GROW, SIZEPOLICY_FIXED, null,
		                                          null, null, 0, false));
		buttonPanel.add(new Spacer(), new GridConstraints(0, 0, 1, 1, ANCHOR_CENTER, FILL_HORIZONTAL,
		                                                  SIZEPOLICY_WANT_GROW, 1, null, null, null, 0, false));
		
		JPanel mainPanel = new JPanel();
		mainPanel.setLayout(new GridLayoutManager(3, 1, new Insets(10, 10, 10, 10), -1, -1));
		mainPanel.add(infoPanel, new GridConstraints(0, 0, 1, 1, ANCHOR_CENTER, FILL_BOTH,
		                                             SIZEPOLICY_CAN_SHRINK | SIZEPOLICY_CAN_GROW,
		                                             SIZEPOLICY_CAN_SHRINK | SIZEPOLICY_CAN_GROW, null, null, null, 0,
				                                     false));
		mainPanel.add(buttonPanel, new GridConstraints(2, 0, 1, 1, ANCHOR_CENTER, FILL_BOTH,
		                                               SIZEPOLICY_CAN_SHRINK | SIZEPOLICY_CAN_GROW,
		                                               SIZEPOLICY_CAN_SHRINK | SIZEPOLICY_CAN_GROW, null, null, null, 0,
				                                       false));
		mainPanel.add(new Spacer(), new GridConstraints(1, 0, 1, 1, ANCHOR_CENTER, FILL_VERTICAL, 1,
		                                                SIZEPOLICY_WANT_GROW, null, new Dimension(-1, 10), null, 0,
		                                                false));
		
		btOk.addActionListener(new OkClick(this));
		lbUrl.addMouseListener(new UrlClick());
		
		setTitle("Über Garbage-Collection-Simulator");
		setResizable(false);
		add(mainPanel);
		pack();
		setLocationRelativeTo(null);
		setModal(true);
		setVisible(true);
	}
	
	/**
	 * Interne Klasse zur Behandlung von Button-Klicks (OK-Button).
	 */
	private class OkClick implements ActionListener {
		private JDialog dialog;
		
		OkClick(JDialog dialog) {
			this.dialog = dialog;
		}
		
		public void actionPerformed(ActionEvent ae) {
			dialog.setVisible(false);
		}
	}
	
	/**
	 * Interne Klasse zur Behandlung von Label-Klicks (URL zum Repository öffnen).
	 */
	private class UrlClick extends MouseAdapter {
		public void mouseClicked(MouseEvent me) {
			try {
				Desktop desktop = Desktop.getDesktop();
				URI url = new URI(Settings.REPOSITORY_URL);
				desktop.browse(url);
			} catch (Exception ignored) {
			}
		}
	}
}