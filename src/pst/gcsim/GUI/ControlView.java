/*
 * Copyright 2019 Phil Steinhorst
 *
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package pst.gcsim.GUI;

import com.intellij.uiDesigner.core.GridConstraints;
import com.intellij.uiDesigner.core.GridLayoutManager;
import pst.gcsim.Controllers.CollectionController;
import pst.gcsim.Launcher;
import pst.gcsim.Settings;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;

import static com.intellij.uiDesigner.core.GridConstraints.*;

/**
 * Formular zur Realisierung der Steuermöglichkeiten des Simulators.
 *
 * @author Phil Steinhorst
 */
public class ControlView extends JFrame {
	/**
	 * Anbindung an Steuerklasse
	 */
	private CollectionController controller;
	/**
	 * Statusanzeigebereich.
	 */
	private JPanel statusPanel;
	
	/**
	 * Statustext.
	 */
	private JLabel lbStatus;
	
	/**
	 * Eingabefelder.
	 */
	private JTextField edMin, edMax, edConnectivity, edDeletion, edRoots, edAnimation;
	
	/**
	 * Schaltflächen.
	 */
	private JButton btCreate, btFill, btCollect, btClear, btCancel, btCanvas;
	
	/**
	 * Konstruktor zur Erzeugung des Fensters.
	 *
	 * @param title      Titel des Fensters (i.d.R. Name des Garbage Collectors)
	 * @param controller Steuerklasse
	 */
	public ControlView(String title, CollectionController controller) {
		super(title);
		this.controller = controller;
		buildView();
	}
	
	/**
	 * Methode zum Aufbau des Fensters.
	 */
	private void buildView() {
		Dimension lbDimension = new Dimension(200, -1);
		Dimension btDimension = new Dimension(160, -1);
		Dimension statusDimension = new Dimension(450, -1);
		Dimension edDimension = new Dimension(60, -1);
		
		JLabel lbMin = new JLabel();
		lbMin.setText("Untere Grenze:");
		lbMin.setToolTipText("Untere Grenze für Größe neuer Objekte");
		edMin = new JTextField();
		edMin.setText("" + Settings.MIN_OBJECT_SIZE);
		edMin.setToolTipText("Untere Grenze für Größe neuer Objekte");
		JLabel lbMax = new JLabel();
		lbMax.setText("Obere Grenze:");
		lbMax.setToolTipText("Obere Grenze für Größe neuer Objekte");
		edMax = new JTextField();
		edMax.setText("" + Settings.MAX_OBJECT_SIZE);
		edMax.setToolTipText("Obere Grenze für Größe neuer Objekte");
		
		btCreate = new JButton();
		btCreate.setText("Objekt erstellen");
		btCreate.setToolTipText("Erstelle einzelnes neues Objekt");
		btFill = new JButton();
		btFill.setText("Heap füllen");
		btFill.setToolTipText("Fülle den Heap, bis Allokation fehlschlägt");
		btCollect = new JButton();
		btCollect.setText("GC ausführen");
		if (!controller.hasCollector()) {
			btCollect.setToolTipText("Keine Garbage Collection eingerichtet.");
			btCollect.setEnabled(false);
		} else btCollect.setToolTipText("Löst einen Garbage-Collection-Zyklus aus.");
		btClear = new JButton();
		btClear.setText("Heap leeren");
		btClear.setToolTipText("Entfernt alle Objekte aus dem Heap.");
		btCanvas = new JButton();
		btCanvas.setText("Heap ausblenden");
		btCanvas.setToolTipText("Versteckt die Zeichenfläche.");
		btCancel = new JButton();
		btCancel.setText("Abbrechen");
		btCancel.setToolTipText("Bricht die aktuelle Aktion ab.");
		btCancel.setEnabled(false);
		
		JLabel lbConnectivity = new JLabel();
		lbConnectivity.setText("Verzweigungsgrad:");
		lbConnectivity.setToolTipText("Wahrscheinlichkeit, mit der zwischen zwei Objekten eine Referenz besteht");
		edConnectivity = new JTextField();
		edConnectivity.setText("" + Settings.CONNECTIVITY);
		edConnectivity.setToolTipText("Wahrscheinlichkeit, mit der zwischen zwei Objekten eine Referenz besteht");
		JLabel lbDeletion = new JLabel();
		lbDeletion.setText("Verwaisungsgrad:");
		lbDeletion.setToolTipText("Wahrscheinlichkeit, mit der eine Referenz aufgelöst wird.");
		edDeletion = new JTextField();
		edDeletion.setText("" + Settings.REFERENCE_DELETION);
		edDeletion.setToolTipText("Wahrscheinlichkeit, mit der eine Referenz aufgelöst wird.");
		JLabel lbRoots = new JLabel();
		lbRoots.setText("Anteil an Basisobjekten:");
		lbRoots.setToolTipText("Wahrscheinlichkeit, mit der ein erzeugtes Objekt ein Basisobjekt ist.");
		edRoots = new JTextField();
		edRoots.setText("" + Settings.ROOT_PROBABILITY);
		edRoots.setToolTipText("Wahrscheinlichkeit, mit der ein erzeugtes Objekt ein Basisobjekt ist.");
		JLabel lbAnimation = new JLabel();
		lbAnimation.setText("Animationsgeschwindigkeit:");
		lbAnimation.setToolTipText("Wartezeit zwischen Animationsschritten in Millisekunden.");
		edAnimation = new JTextField();
		edAnimation.setText("" + Settings.ANIMATION_SPEED);
		edAnimation.setToolTipText("Wartezeit zwischen Animationsschritten in Millisekunden.");
		
		lbStatus = new JLabel();
		lbStatus.setText("Bereit.");
		lbStatus.setToolTipText("Aktueller Status des Garbage-Collection-Simulators.");
		
		JPanel objectCreationPanel = new JPanel();
		objectCreationPanel.setLayout(new GridLayoutManager(4, 3, new Insets(5, 5, 5, 5), -1, -1));
		objectCreationPanel.setBorder(BorderFactory.createTitledBorder(BorderFactory.createEtchedBorder(),
		                                                               "Objekt " + "erstellen"));
		objectCreationPanel.add(lbMin, new GridConstraints(0, 0, 1, 1, ANCHOR_WEST, FILL_NONE, SIZEPOLICY_FIXED,
		                                                   SIZEPOLICY_FIXED, lbDimension, lbDimension, lbDimension,
		                                                   0, false));
		objectCreationPanel.add(edMin, new GridConstraints(0, 1, 1, 1, ANCHOR_CENTER, FILL_NONE, SIZEPOLICY_FIXED,
		                                                   SIZEPOLICY_FIXED, edDimension, edDimension, edDimension,
		                                                   0, false));
		objectCreationPanel.add(lbMax, new GridConstraints(1, 0, 1, 1, ANCHOR_WEST, FILL_NONE, SIZEPOLICY_FIXED,
		                                                   SIZEPOLICY_FIXED, lbDimension, lbDimension, lbDimension,
		                                                   0, false));
		objectCreationPanel.add(edMax, new GridConstraints(1, 1, 1, 1, ANCHOR_WEST, FILL_NONE, SIZEPOLICY_FIXED,
		                                                   SIZEPOLICY_FIXED, edDimension, edDimension, edDimension,
		                                                   0, false));
		objectCreationPanel.add(lbConnectivity, new GridConstraints(2, 0, 1, 1, ANCHOR_WEST, FILL_NONE,
		                                                            SIZEPOLICY_FIXED, SIZEPOLICY_FIXED, lbDimension,
		                                                            lbDimension, lbDimension, 0, false));
		objectCreationPanel.add(edConnectivity, new GridConstraints(2, 1, 1, 1, ANCHOR_CENTER, FILL_NONE,
		                                                            SIZEPOLICY_FIXED, SIZEPOLICY_FIXED, edDimension,
		                                                            edDimension, edDimension, 0, false));
		objectCreationPanel.add(lbRoots, new GridConstraints(3, 0, 1, 1, ANCHOR_WEST, FILL_NONE, SIZEPOLICY_FIXED,
		                                                     SIZEPOLICY_FIXED, lbDimension, lbDimension, lbDimension,
		                                                     0, false));
		objectCreationPanel.add(edRoots, new GridConstraints(3, 1, 1, 1, ANCHOR_CENTER, FILL_NONE, SIZEPOLICY_FIXED,
		                                                     SIZEPOLICY_FIXED, edDimension, edDimension, edDimension,
		                                                     0, false));
		objectCreationPanel.add(btCreate, new GridConstraints(0, 2, 1, 1, ANCHOR_CENTER, FILL_NONE, SIZEPOLICY_FIXED,
		                                                      SIZEPOLICY_FIXED, btDimension, btDimension, btDimension,
		                                                      0, false));
		objectCreationPanel.add(btFill, new GridConstraints(1, 2, 1, 1, ANCHOR_CENTER, FILL_NONE, SIZEPOLICY_FIXED,
		                                                    SIZEPOLICY_FIXED, btDimension, btDimension, btDimension,
		                                                    0, false));
		objectCreationPanel.add(btClear, new GridConstraints(2, 2, 1, 1, ANCHOR_CENTER, FILL_NONE, SIZEPOLICY_FIXED,
		                                                     SIZEPOLICY_FIXED, btDimension, btDimension, btDimension,
		                                                     0, false));
		objectCreationPanel.add(btCanvas, new GridConstraints(3, 2, 1, 1, ANCHOR_CENTER, FILL_NONE, SIZEPOLICY_FIXED,
		                                                      SIZEPOLICY_FIXED, btDimension, btDimension, btDimension,
		                                                      0, false));
		
		JPanel gcPanel = new JPanel();
		gcPanel.setLayout(new GridLayoutManager(2, 3, new Insets(5, 5, 5, 5), -1, -1));
		gcPanel.setBorder(BorderFactory.createTitledBorder(BorderFactory.createEtchedBorder(), "Garbage Collection"));
		gcPanel.add(lbDeletion, new GridConstraints(0, 0, 1, 1, ANCHOR_WEST, FILL_NONE, SIZEPOLICY_FIXED,
		                                            SIZEPOLICY_FIXED, lbDimension, lbDimension, lbDimension, 0, false));
		gcPanel.add(edDeletion, new GridConstraints(0, 1, 1, 1, ANCHOR_CENTER, FILL_NONE, SIZEPOLICY_FIXED,
		                                            SIZEPOLICY_FIXED, edDimension, edDimension, edDimension, 0, false));
		gcPanel.add(lbAnimation, new GridConstraints(1, 0, 1, 1, ANCHOR_WEST, FILL_NONE, SIZEPOLICY_FIXED,
		                                             SIZEPOLICY_FIXED, lbDimension, lbDimension, lbDimension, 0, false));
		gcPanel.add(edAnimation, new GridConstraints(1, 1, 1, 1, ANCHOR_CENTER, FILL_NONE, SIZEPOLICY_FIXED,
		                                             SIZEPOLICY_FIXED, edDimension, edDimension, edDimension, 0, false));
		gcPanel.add(btCollect, new GridConstraints(0, 2, 1, 1, ANCHOR_CENTER, FILL_NONE, SIZEPOLICY_FIXED,
		                                           SIZEPOLICY_FIXED, btDimension, btDimension, btDimension, 0, false));
		gcPanel.add(btCancel, new GridConstraints(1, 2, 1, 1, ANCHOR_CENTER, FILL_NONE, SIZEPOLICY_FIXED,
		                                          SIZEPOLICY_FIXED, btDimension, btDimension, btDimension, 0, false));
		
		statusPanel = new JPanel();
		statusPanel.setLayout(new GridLayoutManager(1, 1, new Insets(5, 5, 5, 5), -1, -1));
		statusPanel.setBorder(BorderFactory.createTitledBorder(BorderFactory.createEtchedBorder(), "Status"));
		statusPanel.add(lbStatus, new GridConstraints(0, 0, 1, 1, ANCHOR_CENTER, FILL_NONE, SIZEPOLICY_FIXED,
		                                              SIZEPOLICY_FIXED, statusDimension, statusDimension,
		                                              statusDimension, 0, false));
		
		// GUI-Komponenten
		JPanel mainPanel = new JPanel();
		mainPanel.setLayout(new GridLayoutManager(3, 1, new Insets(5, 5, 5, 5), -1, -1));
		mainPanel.add(objectCreationPanel, new GridConstraints(0, 0, 1, 1, ANCHOR_CENTER, FILL_BOTH,
		                                                       SIZEPOLICY_CAN_SHRINK | SIZEPOLICY_CAN_GROW,
		                                                       SIZEPOLICY_CAN_SHRINK | SIZEPOLICY_CAN_GROW,
		                                                       null, null, null, 0, false));
		mainPanel.add(gcPanel, new GridConstraints(1, 0, 1, 1, ANCHOR_CENTER, FILL_BOTH,
		                                           SIZEPOLICY_CAN_SHRINK | SIZEPOLICY_CAN_GROW,
		                                           SIZEPOLICY_CAN_SHRINK | SIZEPOLICY_CAN_GROW,
		                                           null, null, null, 0, false));
		mainPanel.add(statusPanel, new GridConstraints(2, 0, 1, 1, ANCHOR_CENTER, FILL_BOTH,
		                                               SIZEPOLICY_CAN_SHRINK | SIZEPOLICY_CAN_GROW,
		                                               SIZEPOLICY_CAN_SHRINK | SIZEPOLICY_CAN_GROW,
		                                               null, null, null, 0, false));
		
		btCreate.addActionListener(new ObjectCreateClick());
		btFill.addActionListener(new HeapFillClick());
		btCollect.addActionListener(new GarbageCollectionClick());
		btClear.addActionListener(new ClearClick());
		btCanvas.addActionListener(new CanvasClick());
		btCancel.addActionListener(new CancelClick());
		
		edMin.addKeyListener(new TextFieldInput(edMin, 4));
		edMax.addKeyListener(new TextFieldInput(edMax, 4));
		edAnimation.addKeyListener(new TextFieldInput(edAnimation, 4));
		edRoots.addKeyListener(new TextFieldInput(edRoots, 6));
		edConnectivity.addKeyListener(new TextFieldInput(edConnectivity, 6));
		edDeletion.addKeyListener(new TextFieldInput(edDeletion, 6));
		
		setResizable(false);
		add(mainPanel);
		pack();
		setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
		setLocation(50, 50);
		setVisible(true);
		addWindowListener(new WindowClose());
	}
	
	/**
	 * Methode zur (De-)Aktivierung der Eingabekomponenten während bzw. nach abgeschlossenem Vorgang.
	 *
	 * @param state {@code true}: Aktiviert alle Komponenten außer dem Abbruchbutton.
	 *              {@code false}: Aktiviert lediglich den Abbruchbutton.
	 */
	public void setInputs(boolean state) {
		btFill.setEnabled(state);
		btCreate.setEnabled(state);
		btCollect.setEnabled(state && controller.hasCollector());
		btClear.setEnabled(state);
		btCancel.setEnabled(!state);
		edMin.setEnabled(state);
		edMax.setEnabled(state);
		edConnectivity.setEnabled(state);
		edDeletion.setEnabled(state);
		edRoots.setEnabled(state);
		edAnimation.setEnabled(state);
	}
	
	/**
	 * Testet Formulareingaben auf Plausibilität.
	 *
	 * @return {@code true}, wenn Eingaben gültig, sonst {@code false}.
	 */
	private boolean applyInputs() {
		int min, max, animation;
		double connectivity, deletion, rootProb;
		// Teste Formate der Eingaben
		try {
			min = Integer.parseInt(edMin.getText());
		} catch (NumberFormatException e) {
			String msg = "Ungültiges Zahlformat für untere Schranke: " + edMin.getText();
			setStatus(msg, true);
			JOptionPane.showMessageDialog(this, msg, "Fehler", JOptionPane.ERROR_MESSAGE);
			return false;
		}
		
		try {
			max = Integer.parseInt(edMax.getText());
		} catch (NumberFormatException e) {
			String msg = "Ungültiges Zahlformat für obere Schranke: " + edMax.getText();
			setStatus(msg, true);
			JOptionPane.showMessageDialog(this, msg, "Fehler", JOptionPane.ERROR_MESSAGE);
			return false;
		}
		
		try {
			animation = Integer.parseInt(edAnimation.getText());
		} catch (NumberFormatException e) {
			String msg = "Ungültiges Zahlformat für Animationsgeschwindigkeit: " + edAnimation.getText();
			setStatus(msg, true);
			JOptionPane.showMessageDialog(this, msg, "Fehler", JOptionPane.ERROR_MESSAGE);
			return false;
		}
		
		try {
			connectivity = Double.parseDouble(edConnectivity.getText());
		} catch (NumberFormatException e) {
			String msg = "Ungültiges Zahlformat für Konnektivität: " + edConnectivity.getText();
			setStatus(msg, true);
			JOptionPane.showMessageDialog(this, msg, "Fehler", JOptionPane.ERROR_MESSAGE);
			return false;
		}
		
		try {
			deletion = Double.parseDouble(edDeletion.getText());
		} catch (NumberFormatException e) {
			String msg = "Ungültiges Zahlformat für Verwaisungsgrad: " + edDeletion.getText();
			setStatus(msg, true);
			JOptionPane.showMessageDialog(this, msg, "Fehler", JOptionPane.ERROR_MESSAGE);
			return false;
		}
		
		try {
			rootProb = Double.parseDouble(edRoots.getText());
		} catch (NumberFormatException e) {
			String msg = "Ungültiges Zahlformat für Anteil Basisobjekte: " + edRoots.getText();
			setStatus(msg, true);
			JOptionPane.showMessageDialog(this, msg, "Fehler", JOptionPane.ERROR_MESSAGE);
			return false;
		}
		
		// Teste auf sinnvoll gewählte Grenzen für Objektgröße
		if (min < 1 || max < 1) {
			String msg = "Objektgröße: Bitte nur positive Zahlen eingeben.";
			setStatus(msg, true);
			JOptionPane.showMessageDialog(this, msg, "Fehler", JOptionPane.ERROR_MESSAGE);
			return false;
		}
		
		// Teste auf sinnvolle Animationsgeschwindigkeit
		if (animation < 1 || animation > 1000) {
			String msg = "Animationsdauer: Bitte nur Zahlen zwischen 1 und 1000 eingeben.";
			setStatus(msg, true);
			JOptionPane.showMessageDialog(this, msg, "Fehler", JOptionPane.ERROR_MESSAGE);
			return false;
		}
		
		// tausche ggfs. Minimum und Maximum
		if (min > max) {
			String tmp = edMin.getText();
			edMin.setText(edMax.getText());
			edMax.setText(tmp);
			min = min ^ max;
			max = min ^ max;
			min = min ^ max;
		}
		
		// Teste auf sinnvolle Wahrscheinlichkeiten
		if (connectivity < 0 || connectivity > 1) {
			String msg = "Verzweigungsgrad: Bitte nur Zahlen zwischen 0 und 1 eingeben.";
			setStatus(msg, true);
			JOptionPane.showMessageDialog(this, msg, "Fehler", JOptionPane.ERROR_MESSAGE);
			return false;
		}
		if (deletion < 0 || deletion > 1) {
			String msg = "Verwaisungsgrad: Bitte nur Zahlen zwischen 0 und 1 eingeben.";
			setStatus(msg, true);
			JOptionPane.showMessageDialog(this, msg, "Fehler", JOptionPane.ERROR_MESSAGE);
			return false;
		}
		// Teste auf sinnvolle Wahrscheinlichkeiten
		if (rootProb < 0 || rootProb > 1) {
			String msg = "Basisobjekte: Bitte nur Zahlen zwischen 0 und 1 eingeben.";
			setStatus(msg, true);
			JOptionPane.showMessageDialog(this, msg, "Fehler", JOptionPane.ERROR_MESSAGE);
			return false;
		}
		
		// Übernehme Konfiguration
		Settings.MIN_OBJECT_SIZE = min;
		Settings.MAX_OBJECT_SIZE = max;
		Settings.CONNECTIVITY = connectivity;
		Settings.REFERENCE_DELETION = deletion;
		Settings.ROOT_PROBABILITY = rootProb;
		Settings.ANIMATION_SPEED = animation;
		return true;
	}
	
	/**
	 * Setzt den Text im Statusbereich.
	 *
	 * @param msg      Nachricht
	 * @param critical {@code true}, wenn kritischer Fehler, sonst {@code false}.
	 */
	public void setStatus(String msg, boolean critical) {
		if (critical) {
			statusPanel.setBackground(Color.RED);
			lbStatus.setForeground(Color.WHITE);
		} else {
			statusPanel.setBackground(new Color(242, 242, 242));
			lbStatus.setForeground(Color.BLACK);
		}
		lbStatus.setText(msg);
		Launcher.log(msg, critical);
	}
	
	/**
	 * Interne Klasse zur Behandlung von Button-Klicks (Objekt erstellen).
	 */
	private class ObjectCreateClick implements ActionListener {
		public void actionPerformed(ActionEvent ae) {
			if (!applyInputs()) return;
			controller.createObject();
		}
	}
	
	/**
	 * Interne Klasse zur Behandlung von Button-Klicks (Heap auffüllen).
	 */
	private class HeapFillClick implements ActionListener {
		public void actionPerformed(ActionEvent ae) {
			if (!applyInputs()) return;
			controller.fillHeap();
		}
	}
	
	/**
	 * Interne Klasse zur Behandlung von Button-Klicks (GC auslösen).
	 */
	private class GarbageCollectionClick implements ActionListener {
		public void actionPerformed(ActionEvent ae) {
			if (!applyInputs()) return;
			controller.collectGarbage();
		}
	}
	
	/**
	 * Interne Klasse zur Behandlung von Button-Klicks (Heap leeren).
	 */
	private class ClearClick implements ActionListener {
		public void actionPerformed(ActionEvent ae) {
			controller.clearHeap();
			setInputs(true);
		}
	}
	
	/**
	 * Interne Klasse zur Behandlung von Button-Klicks (Canvas verstecken).
	 */
	private class CanvasClick implements ActionListener {
		public void actionPerformed(ActionEvent ae) {
			if (controller.getCanvasVisibility()) {
				controller.setCanvasVisibility(false);
				btCanvas.setText("Heap anzeigen");
				btCanvas.setToolTipText("Blendet die Zeichenfläche wieder ein.");
			} else {
				controller.setCanvasVisibility(true);
				btCanvas.setText("Heap ausblenden");
				btCanvas.setToolTipText("Versteckt die Zeichenfläche.");
			}
		}
	}
	
	/**
	 * Interne Klasse zur Behandlung von Button-Klicks (unterbrechen).
	 */
	private class CancelClick implements ActionListener {
		public void actionPerformed(ActionEvent ae) {
			controller.cancel();
			setStatus("Aktion abgebrochen. Heap in inkonsistentem Zustand.", true);
			btClear.setEnabled(true);
			btCancel.setEnabled(false);
		}
	}
	
	/**
	 * Interne Klasse zur Behandlung des Fensterschließens (zurück zum Auswahlfenster).
	 */
	private class WindowClose extends WindowAdapter {
		public void windowClosing(WindowEvent we) {
			controller.exit();
		}
	}
	
	/**
	 * Interne Klasse zur Behandlung von Texteingaben (Resultate berechnen).
	 */
	private class TextFieldInput implements KeyListener {
		private int chars;
		private JTextField tf;
		
		TextFieldInput(JTextField tf, int chars) {
			this.tf = tf;
			this.chars = chars;
		}
		
		public void keyTyped(KeyEvent ae) {
			if (tf.getText().length() > chars) tf.setText(tf.getText().substring(0, chars));
		}
		
		public void keyPressed(KeyEvent ae) {keyTyped(ae);}
		
		public void keyReleased(KeyEvent ae) {keyTyped(ae);}
	}
}