/*
 * Copyright 2019 Phil Steinhorst
 *
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package pst.gcsim;

import pst.gcsim.GUI.GcSelectionView;

import javax.swing.*;
import java.text.SimpleDateFormat;

/**
 * Hauptklasse zum Starten der Anwendung.
 *
 * @author Phil Steinhorst
 */
public class Launcher {
	/**
	 * Datums- und Zeitformat zur Ausgabe von Meldungen in der Konsole.
	 */
	private static final SimpleDateFormat timeFormat = new SimpleDateFormat("yyyy-MM-dd_HH:mm:ss.SSS");
	
	/**
	 * Methode zum Start der Anwendung.
	 * @param args Kommandozeilenparameter.
	 */
	public static void main(String[] args) {
		log("Starte Garbage-Collection-Simulator.", false);
		if (args.length == 0)
			Settings.readSettings();
		else if (args.length == 1 && !args[0].equals("default")) {
			String msg = "Ungültiger Parameter übergeben: " + args[0];
			log(msg, true);
			JOptionPane.showMessageDialog(null, msg, "Fehler", JOptionPane.ERROR_MESSAGE);
			return;
		} else if (args.length > 1) {
			String msg = "Ungültige Anzahl Parameter übergeben: " + args.length;
			log(msg, true);
			JOptionPane.showMessageDialog(null, msg, "Fehler", JOptionPane.ERROR_MESSAGE);
			return;
		} else log("Verwende Standardeinstellungen.", false);
		new GcSelectionView();
	}
	
	/**
	 * Methode zur Ausgabe von Meldungen in der Konsole.
	 * @param msg       Auszugebende Nachricht.
	 * @param critical  {@code true}, falls kritische Nachricht, sonst {@code false}.
	 */
	public static void log(String msg, boolean critical) {
		System.out.println((critical ? " ! " : "   ") + timeFormat.format(System.currentTimeMillis()) + "   " + msg);
	}
}