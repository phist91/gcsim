/*
 * Copyright 2019 Phil Steinhorst
 *
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package pst.gcsim.Allocators;
import pst.gcsim.Objects.HeapObject;

/**
 * Naiver Allokator zur Erfüllung von Speicheranforderungen. Sucht ausgehend vom zuletzt angelegten Objekt die
 * nächstbeste freie Stelle im Speicher.
 *
 * @author Phil Steinhorst
 */
public class NaiveAllocator implements Allocator {
	/**
	 * Anzahl der Heapwörter.
	 */
	int heapSize;
	
	/**
	 * Positionszeiger.
	 */
	int position;
	
	/**
	 * Bitvektor zur Speicherung des Belegungszustandes.
	 */
	AllocationBitmap allocationBitmap;
	
	/**
	 * Konstruktor zur Initialisierung des Allokators anhand der Heapgröße.
	 *
	 * @param heapSize Größe des Heaps in Wörtern.
	 */
	public NaiveAllocator(int heapSize) {
		this(heapSize, 0, new AllocationBitmap(heapSize));
	}
	
	/**
	 * Allgemeiner Konstruktor.
	 *
	 * @param heapSize         Größe des Heaps in Wörtern.
	 * @param position         Aktuelle Position des Allokators.
	 * @param allocationBitmap Vorgefertigte Allocation Bitmap.
	 */
	NaiveAllocator(int heapSize, int position, AllocationBitmap allocationBitmap) {
		if (heapSize < 0)
			throw new IllegalArgumentException("Ungültige Heapgröße angegeben: " + heapSize);
		this.heapSize = heapSize;
		if (position < 0 || position >= heapSize)
			throw new IllegalArgumentException("Ungültige Startposition angegeben: " + position);
		this.position = position;
		if (allocationBitmap == null)
			throw new IllegalArgumentException("Ungültige Allocation Bitmap angegeben: null");
		this.allocationBitmap = allocationBitmap;
	}
	
	/**
	 * Konstruktor zur Initialisierung anhand eines existierenden {@code boolean}-Arrays (hauptsächlich zu
	 * Testzwecken).
	 *
	 * @param bitmap   {@code boolean}-Array, das die Belegung des Heaps anzeigt.
	 * @param position Aktuelle Position des Allokators.
	 */
	NaiveAllocator(boolean[] bitmap, int position) {
		this(bitmap.length, position, new AllocationBitmap(bitmap));
	}
	
	/**
	 * Methode zur Erzeugung eines Heapobjekts. Veranlasst die Allokation von Heapspeicher und ggfs. die Ausläsung der
	 * Garbage Collection.
	 *
	 * @param size Größe des neuen Objekts.
	 * @return Neues Objekt, das seine Position im Heap enthält. {@code null}, falls kein Objekt angelegt werden
	 * 		konnte.
	 */
	public HeapObject newObject(int size) {
		if (size < 1)
			throw new IllegalArgumentException("Ungültige Objektgröße angegeben: " + size);
		int address = allocate(size);
		if (address == -1) {
			return null;
		}
		return new HeapObject(address, size);
	}
	
	/**
	 * Methode zur Freigabe eines Objekts und des belegten Heapspeichers.
	 *
	 * @param obj Objekt, das aus dem Heap entfernt werden soll.
	 */
	public void free(HeapObject obj) {
		if (obj == null || (obj.getAddress() + obj.getSize() > heapSize))
			throw new IllegalArgumentException("Ungültiges Objekt angegeben.");
		for (int i = obj.getAddress(); i < obj.getAddress() + obj.getSize(); i++)
			allocationBitmap.setBit(i, false);
	}
	
	/**
	 * Liefert Attribut {@code heapSize} zurück.
	 *
	 * @return {@code heapSize}
	 */
	public int getHeapSize() {
		return heapSize;
	}
	
	/**
	 * Verschiebt ein Objekt innerhalb des Heaps.
	 *
	 * @param obj     zu verschiebendes Objekt
	 * @param address Zieladresse
	 */
	public void moveObject(HeapObject obj, int address) {
		if (obj == null || (obj.getAddress() + obj.getSize() > heapSize))
			throw new IllegalArgumentException("Ungültiges Objekt angegeben.");
		if (address < 0 || address > heapSize - obj.getSize())
			throw new IllegalArgumentException("Ungültige Zieladresse angegeben: " + address);
		// Bitvektor anpassen
		for (int i = obj.getAddress(); i < obj.getAddress() + obj.getSize(); i++)
			allocationBitmap.setBit(i, false);
		obj.setAddress(address);
		for (int i = address; i < address + obj.getSize(); i++)
			allocationBitmap.setBit(i, true);
	}
	
	/**
	 * Setzt einen Bereich des Heaps auf nicht belegt.
	 *
	 * @param first Erster Index des zu setzenden Bereichs
	 * @param last  Letzter Index des zu setzenden Bereichs
	 */
	public void clearRange(int first, int last) {
		if (first > last || first < 0 || last >= heapSize)
			throw new IllegalArgumentException("Ungültige Indizes übergeben: " + first + " und " + last);
		for (int i = first; i <= last; i++)
			allocationBitmap.setBit(i, false);
	}
	
	/**
	 * Liefert Attribut {@code position} zurück.
	 *
	 * @return {@code position}
	 */
	public int getPosition() {
		return position;
	}
	
	/**
	 * Setzt die aktuelle Position des Allokators.
	 *
	 * @param position Neue Position des Allokators.
	 */
	public void setPosition(int position) {
		if (position < 0 || position >= heapSize)
			throw new IllegalArgumentException("Ungültiger Wert für neue Allokatorposition: " + position);
		this.position = position;
	}
	
	/**
	 * Setzt den Allokator zurück und markiert den gesamten Heap als unbelegt.
	 */
	public void reset() {
		position = 0;
		clearRange(0, heapSize - 1);
	}
	
	/**
	 * Methode zur Allokation von Heapspeicher. Wird von {@code newObject} aufgerufen.
	 *
	 * @param size Zu allozierende Speichermenge.
	 * @return Adresse des reservierten Speicherbereichs (-1, falls Allokation fehlschlägt)
	 */
	int allocate(int size) {
		int gapSize = 0;        // Größe der aktuell betrachteten Lücke
		int end = position;     // Beginne Allokationsversuch bei aktueller Position
		int start = position;
		
		// durchsuche Heap hinter position, solange noch keine hinreichend große Lücke gefunden
		// und noch nicht am Heapende angekommen
		while (gapSize < size && end != heapSize) {
			start = allocationBitmap.nextFree(end);
			if (start == -1)    // Am Heapende angekommen --> nicht genügend freier Speicher hinter position
				break;
			end = allocationBitmap.nextUsed(start);
			if (end == -1)      // Am Heapende angekommen
				end = heapSize;
			gapSize = end - start;
		}
		
		// Prüfe, ob gefundene Lücke groß genug
		if (gapSize >= size) {       // zutreffend, falls genügend freier Speicher hinter position gefunden
			position = (start + size) % heapSize;       // position anpassen (evtl. an Heapanfang schieben)
			for (int i = start; i < start + size; i++)  // Bitvektor anpassen
				allocationBitmap.setBit(i, true);
			return start;
		}
		
		// Keine hinreichend große Lücke gefunden; setze Suche am Heapanfang fort.
		start = 0;
		end = 0;
		// durchsuche Heap vor position, solange noch keine hinreichend große Lücke gefunden
		// und noch nicht an position vorbeigezogen
		while (gapSize < size && start < position) {
			start = allocationBitmap.nextFree(end);
			if (start == -1)    // Am Heapende angekommen --> nicht genügend freier Speicher vor position
				break;
			end = allocationBitmap.nextUsed(start);
			if (end == -1)      // Am Heapende angekommen
				end = heapSize;
			gapSize = end - start;
		}
		
		if (gapSize >= size) {       // zutreffend, falls genügend freier Speicher hinter position gefunden
			position = (start + size) % heapSize;       // position anpassen (evtl. an Heapanfang schieben)
			for (int i = start; i < start + size; i++)  // Bitvektor anpassen
				allocationBitmap.setBit(i, true);
			return start;
		}
		
		return -1;      // keine hinreichend große Lücke gefunden
	}
	
	/**
	 * Methode zur Erzeugung eines Heapobjekts an einer vorgegebenen Stelle. Schlägt fehl, wenn die angegebene Stelle
	 * nicht groß genug bzw. bereits belegt ist.
	 *
	 * @param size    Größe des neuen Objekts.
	 * @param address Gewünschte Speicheradresse des neuen Objekts.
	 * @return Neues Objekt, das seine Position im Heap enthält.
	 */
	public HeapObject newObject(int size, int address) {
		if (size < 1 || size + address > heapSize)
			throw new IllegalArgumentException("Ungültige Objektgröße angegeben: " + size);
		if (address < 0 || address > heapSize - 1)
			throw new IllegalArgumentException("Ungültige Speicheradresse angegeben: " + address);
		for (int i = address; i < address + size; i++)
			if (allocationBitmap.getBit(i))
				throw new IllegalArgumentException("Nicht genügend freier Speicher an angegebener Speicheradresse " +
						                                   "vorhanden: " + address);
		for (int i = address; i < address + size; i++)
			allocationBitmap.setBit(i, true);
		return new HeapObject(address, size);
	}
	
	/**
	 * Liefert die {@code AllocationBitmap} des Allokators.
	 *
	 * @return {@code allocationBitmap}
	 */
	public AllocationBitmap getAllocationBitmap() {
		return allocationBitmap;
	}
}