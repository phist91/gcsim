/*
 * Copyright 2019 Phil Steinhorst
 *
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package pst.gcsim.Allocators;
import pst.gcsim.Objects.HeapObject;

/**
 * Schnittstelle für Allokatoren.
 *
 * @author Phil Steinhorst
 */
public interface Allocator {
	/**
	 * Methode zur Anforderung von Speicher durch den Mutator. Erzeugt neues Heapobjekt.
	 *
	 * @param size Größe des neuen Objekts.
	 * @return Heapobjekt gewünschter Größe.
	 */
	HeapObject newObject(int size);
	
	/**
	 * Methode zur Freigabe eines Objekts.
	 *
	 * @param obj Objekt, das aus dem Heap entfernt werden soll.
	 */
	void free(HeapObject obj);
	
	/**
	 * Liefert Größe des Heaps.
	 *
	 * @return Größe des Heaps.
	 */
	int getHeapSize();
	
	/**
	 * Verschiebt ein Objekt innerhalb des Heaps.
	 *
	 * @param obj     zu verschiebendes Objekt.
	 * @param address Zieladresse.
	 */
	void moveObject(HeapObject obj, int address);
	
	/**
	 * Setzt einen Bereich des Heaps auf nicht belegt.
	 *
	 * @param first Erster Index des zu setzenden Bereichs.
	 * @param last  Letzter Index des zu setzenden Bereichs.
	 */
	void clearRange(int first, int last);
	
	/**
	 * Liefert aktuelle Position des Allokators.
	 *
	 * @return Position des Allokators.
	 */
	int getPosition();
	
	/**
	 * Setzt Position des Allokators auf eine bestimmte Stelle.
	 *
	 * @param position Neue Position des Allokators.
	 */
	void setPosition(int position);
	
	/**
	 * Setzt den Allokator zurück und markiert den gesamten Heap als unbelegt.
	 */
	void reset();
}