/*
 * Copyright 2019 Phil Steinhorst
 *
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package pst.gcsim.Allocators;

/**
 * Allokator für einen in Halbräumen aufgeteilten Heap.
 *
 * @author Phil Steinhorst
 */
public class SemispaceAllocator extends NaiveAllocator {
	/**
	 * Zeiger auf Halbraum, in dem aktuell Objekte angelegt werden.
	 */
	private int target;
	
	/**
	 * Konstruktor zur Initialisierung des Allokators anhand der Heapgröße und dem Beginn des zweiten Halbraums.
	 *
	 * @param heapSize Größe des Heaps in Wörtern.
	 */
	public SemispaceAllocator(int heapSize) {
		this(heapSize, false, 0, new AllocationBitmap(heapSize));
	}
	
	/**
	 * Allgemeiner Konstruktor.
	 *
	 * @param heapSize         Größe des Heaps in Wörtern
	 * @param flipped          Festlegung des Zielhalbraums    ({@code true}: hinten, {@code false}: vorne)
	 * @param position         Aktuelle Position des Allokators
	 * @param allocationBitmap Vorgefertigte Allocation Bitmap
	 */
	SemispaceAllocator(int heapSize, boolean flipped, int position, AllocationBitmap allocationBitmap) {
		super(heapSize, position, allocationBitmap);
		this.target = flipped ? heapSize / 2 : 0;
		if (position < target || position >= (target == 0 ? heapSize / 2 : heapSize))
			throw new IllegalArgumentException("Startposition befindet sich nicht im Zielhalbraum: " + position);
	}
	
	/**
	 * Konstruktor (hauptsächlich zu Testzwecken)
	 *
	 * @param bitmap   Boolean-Array, das die Belegung des Heaps anzeigt.
	 * @param position Aktuelle Position des Allokators
	 * @param flipped  Festlegung des Zielhalbraums    ({@code true}: hinten, {@code false}: vorne)
	 */
	SemispaceAllocator(boolean[] bitmap, int position, boolean flipped) {
		this(bitmap.length, flipped, position, new AllocationBitmap(bitmap));
	}
	
	/**
	 * Setzt die aktuelle Position des Allokators.
	 *
	 * @param position Neue Position des Allokators.
	 */
	@Override
	public void setPosition(int position) {
		if (position < target || position >= (target == 0 ? heapSize / 2 : heapSize))
			throw new IllegalArgumentException("Ungültiger Wert für neue Allokatorposition: " + position);
		this.position = position;
	}
	
	/**
	 * Setzt den Allokator zurück und markiert den gesamten Heap als unbelegt.
	 */
	public void reset() {
		target = 0;
		position = 0;
		clearRange(0, heapSize - 1);
	}
	
	/**
	 * Methode zur Allokation von Heapspeicher. Wird von {@code newObject} aufgerufen.
	 *
	 * @param size Zu allozierende Speichermenge.
	 * @return Adresse des reservierten Speicherbereichs (-1, falls Allokation fehlschlägt)
	 */
	@Override
	int allocate(int size) {
		if (target == 0)
			return allocateInSemispace(size, heapSize / 2 - 1);
		else
			return allocateInSemispace(size, heapSize - 1);
	}
	
	/**
	 * Interne Methode zur Allokation von Heapspeicher in einem bestimmten Bereich. Wird von {@code allocate}
	 * aufgerufen.
	 *
	 * @param size         Zu allozierende Speichermenge.
	 * @param semispaceEnd Letztes Wort des Bereichs.
	 * @return Adresse des reservierten Speicherbereichs (-1, falls Allokation fehlschlägt)
	 */
	private int allocateInSemispace(int size, int semispaceEnd) {
		int gapSize = 0;
		int end = position;     // Beginne Allokationsversuch bei aktueller Position
		int start = position;
		
		// durchsuche Heap hinter position, solange noch keine hinreichend große Lücke gefunden
		// und noch nicht am Heapende angekommen
		while (gapSize < size && end < semispaceEnd + 1) {
			start = allocationBitmap.nextFree(end);
			if (start == -1 || start > semispaceEnd)    // Am Halbraumende angekommen --> nicht genügend freier
				// Speicher hinter position
				break;
			end = allocationBitmap.nextUsed(start);
			if (end == -1 || end > semispaceEnd)      // Am Heapende angekommen
				end = semispaceEnd + 1;
			gapSize = end - start;
		}
		
		if (gapSize >= size) {       // zutreffend, falls genügend freier Speicher hinter position gefunden
			position = start + size;       // position anpassen (evtl. an Heapanfang schieben)
			if (position == heapSize / 2)
				position = 0;
			if (position == heapSize)
				position = heapSize / 2;
			for (int i = start; i < start + size; i++)  // Bitvektor anpassen
				allocationBitmap.setBit(i, true);
			return start;
		}
		
		start = target;  // beginne am Heapanfang
		end = target;
		// durchsuche Heap vor position, solange noch keine hinreichend große Lücke gefunden
		// und noch nicht an position vorbeigezogen
		while (gapSize < size && start < position) {
			start = allocationBitmap.nextFree(end);
			if (start == -1 || start > semispaceEnd)    // Am Heapende angekommen --> nicht genügend freier Speicher
				// vor position
				break;
			end = allocationBitmap.nextUsed(start);
			if (end == -1 || end > semispaceEnd)      // Am Heapende angekommen
				end = semispaceEnd + 1;
			gapSize = end - start;
		}
		
		if (gapSize >= size) {
			position = start + size;
			if (position == heapSize / 2)
				position = 0;
			if (position == heapSize)
				position = heapSize / 2;
			for (int i = start; i < start + size; i++)
				allocationBitmap.setBit(i, true);
			return start;
		}
		return -1;      // keine hinreichend große Lücke gefunden
	}
	
	/**
	 * Tauscht die Rollen der beiden Halbräume.
	 */
	public void swapSemispaces() {
		if (target == 0) {
			target = heapSize / 2;
			position = heapSize / 2;
		} else {
			target = 0;
			position = 0;
		}
	}
	
	/**
	 * Liefert Beginn des aktuellen Zielhalbraums.
	 *
	 * @return Attribut {@code target}
	 */
	public int getTarget() {
		return target;
	}
	
	/**
	 * Bereinigt den Halbraum, der nicht Zielhalbraum ist.
	 */
	public void clearSource() {
		if (target == 0)
			clearRange(heapSize / 2, heapSize - 1);
		else
			clearRange(0, heapSize / 2 - 1);
	}
}