/*
 * Copyright 2019 Phil Steinhorst
 *
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package pst.gcsim.Allocators;

/**
 * Klasse zur Realisierung eines Bitvektors, der die Belegung der Wörter
 * des Heaps speichert.
 *
 * @author Phil Steinhorst
 */
public class AllocationBitmap {
	/**
	 * Bitvektor zur Speicherung des Belegungszustandes.
	 */
	private boolean[] bitmap;
	/**
	 * Anzahl der Heapwörter.
	 */
	private int size;
	
	/**
	 * Erzeuge {@code AllocationBitmap} gewünschter Größe.
	 *
	 * @param size Länge der Bitmap.
	 */
	public AllocationBitmap(int size) {
		if (size < 1) throw new IllegalArgumentException("Ungültige Bitmap-Länge angegeben: " + size);
		this.size = size;
		bitmap = new boolean[size];
	}
	
	/**
	 * Erzeuge {@code AllocationBitmap} aus Boolean-Array (hauptsächlich zu Testzwecken).
	 *
	 * @param bitmap Boolean-Array.
	 */
	public AllocationBitmap(boolean[] bitmap) {
		if (bitmap == null) throw new IllegalArgumentException("Ungültiger Bitvektor übergeben: null.");
		this.size = bitmap.length;
		this.bitmap = bitmap;
	}
	
	/**
	 * Liefert Wert des Bits mit angegebenem Index.
	 *
	 * @param index Index des Bits.
	 * @return Wert des Bits.
	 */
	public boolean getBit(int index) {
		if (index >= size || index < 0)
			throw new IllegalArgumentException("Ungültiger Index für Bitmap angegeben: " + index);
		return bitmap[index];
	}
	
	/**
	 * Setzt Wert des Bits mit angegebenem Index.
	 *
	 * @param index Index des Bits.
	 * @param value Zu setzender Wert des Bits.
	 */
	public void setBit(int index, boolean value) {
		if (index >= size || index < 0)
			throw new IllegalArgumentException("Ungültiger Index für Bitmap angegeben: " + index);
		bitmap[index] = value;
	}
	
	/**
	 * Gibt den Index des nächsten nicht gesetzten Bits aus.
	 *
	 * @param index Index, ab dem gesucht werden soll.
	 * @return Index des nächsten ungesetzten Bits ab {@code index}, falls existent, sonst -1.
	 */
	public int nextFree(int index) {
		if (index >= size || index < 0)
			throw new IllegalArgumentException("Ungültiger Index für Bitmap angegeben: " + index);
		for (int i = index; i < size; i++)
			if (!bitmap[i]) return i;
		return -1;
	}
	
	/**
	 * Gibt den Index des nächsten gesetzten Bits aus.
	 *
	 * @param index Index, ab dem gesucht werden soll.
	 * @return Index des nächsten gesetzten Bits ab {@code index}, falls existent, sonst -1.
	 */
	public int nextUsed(int index) {
		if (index >= size || index < 0)
			throw new IllegalArgumentException("Ungültiger Index für Bitmap angegeben: " + index);
		for (int i = index; i < size; i++)
			if (bitmap[i]) return i;
		return -1;
	}
	
	/**
	 * Liefert Attribut {@code bitmap} zurück.
	 *
	 * @return {@code bitmap}.
	 */
	public boolean[] getBitmap() {
		return bitmap;
	}
	
	/**
	 * Liefert Attribut {@code size} zurück.
	 *
	 * @return {@code size}.
	 */
	public int getSize() {
		return size;
	}
}