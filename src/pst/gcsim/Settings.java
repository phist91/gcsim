/*
 * Copyright 2019 Phil Steinhorst
 *
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package pst.gcsim;

import pst.gcsim.Allocators.NaiveAllocator;
import pst.gcsim.Allocators.SemispaceAllocator;
import pst.gcsim.Controllers.CollectionController;
import pst.gcsim.GUI.GarbageCollectorSelection;
import pst.gcsim.GarbageCollectors.*;

import java.awt.*;
import java.io.*;
import java.util.Properties;

/**
 * Enthält übergreifende Einstellungen der Anwendung.
 *
 * @author Phil Steinhorst
 */
public class Settings {
	/**
	 * vertikaler Rand.
	 */
	public static final int MARGIN_VERT = 10;
	
	/**
	 * horizontaler Rand.
	 */
	public static final int MARGIN_HORIZ = 10;
	
	/**
	 * Zellenbreite.
	 */
	public static int CELL_WIDTH = 20;
	
	/**
	 * Zellenhöhe.
	 */
	public static int CELL_HEIGHT = 30;
	
	/**
	 * Farbe für verwaiste Objekte (Pantone 312).
	 */
	public static final Color COLOR_DEAD = new Color(0, 157, 209);
	
	/**
	 * Farbe für markierte/entdeckte Objekte (Pantone 396).
	 */
	public static final Color COLOR_MARKED = new Color(255, 237, 0);
	
	/**
	 * Farbe für erreichbare Objekte (Pantone 369).
	 */
	public static final Color COLOR_LIVE = new Color(122, 181, 22);
	
	/**
	 * Farbe für bereits kopierte Objekte (Pantone 315).
	 */
	public static final Color COLOR_COPIED = new Color(0, 110, 137);
	
	/**
	 * Name des Entwicklers.
	 */
	public static final String DEV_NAME = "Phil Steinhorst";
	
	/**
	 * E-Mail-Adresse des Entwicklers.
	 */
	public static final String DEV_MAIL = "p.st@wwu.de";
	
	/**
	 * Link zum GitHub-Repository.
	 */
	public static final String REPOSITORY_URL = "https://www.github.com/phist91/gcsim";
	
	/**
	 * Zuletzt gewählter Garbage Collector.
	 */
	public static int LAST_SELECTION = 0;
	
	/**
	 * Anzahl der Heapspalten.
	 */
	public static int HEAP_COLS = 40;
	
	/**
	 * Anzahl der Heapzeilen.
	 */
	public static int HEAP_ROWS = 20;
	
	/**
	 * Animationsdauer in Millisekunden.
	 */
	public static int ANIMATION_SPEED = 200;
	
	/**
	 * Untere Schranke für Objektgröße.
	 */
	public static int MIN_OBJECT_SIZE = 2;
	
	/**
	 * Obere Straße für Objektgröße.
	 */
	public static int MAX_OBJECT_SIZE = 30;
	
	/**
	 * Verzweigungsgrad.
	 */
	public static double CONNECTIVITY = 0.1;
	
	/**
	 * Verwaisungsgrad.
	 */
	public static double REFERENCE_DELETION = 0.3;
	
	/**
	 * Wahrscheinlichkeit für Basisobjekte.
	 */
	public static double ROOT_PROBABILITY = 0.2;
	
	
	////////////////////////////////////////////////////////////
	////////////// Garbage-Collection-Algorithmen //////////////
	////////////////////////////////////////////////////////////
	
	
	/**
	 * Mark-Sweep-Kollektor.
	 */
	private static GarbageCollectorSelection marksweep = new GarbageCollectorSelection(
			"Mark Sweep",
			() -> new CollectionController<NaiveAllocator>(
					false,
					new NaiveAllocator(Settings.heapSize()),
					new MarkSweep<NaiveAllocator>())
	);
	
	/**
	 * LISP-2-Kollektor.
	 */
	private static GarbageCollectorSelection lisp2 = new GarbageCollectorSelection(
			"Lisp 2",
			() -> new CollectionController<NaiveAllocator>(
					false,
					new NaiveAllocator(Settings.heapSize()),
					new Lisp2<NaiveAllocator>())
	);
	
	/**
	 * Halbraumverfahren.
	 */
	private static GarbageCollectorSelection semispace = new GarbageCollectorSelection(
			"Halbraumverfahren",
			() -> new CollectionController<SemispaceAllocator>(
					false,
					new SemispaceAllocator(Settings.heapSize()),
					new SemispaceCollector<SemispaceAllocator>())
	);
	
	/**
	 * Naiver Allokator (keine Garbage Collection).
	 */
	private static GarbageCollectorSelection naiveAlloc = new GarbageCollectorSelection(
			"Keine Garbage Collection (nur naive Allokation)",
			() -> new CollectionController<NaiveAllocator>(
					false,
					new NaiveAllocator(Settings.heapSize()),
					null)
	);
	
	/**
	 * Halbraum-Allokator (keine Garbage Collection).
	 */
	private static GarbageCollectorSelection semispaceAlloc = new GarbageCollectorSelection(
			"Keine Garbage Collection (nur Halbraum-Allokation)",
			() -> new CollectionController<SemispaceAllocator>(
					false,
					new SemispaceAllocator(Settings.heapSize()),
					null)
	);
	
	/**
	 * Zur Verfügung stehende Kollektoren.
	 */
	public static final GarbageCollectorSelection[] collectors = {
			marksweep, lisp2, semispace, naiveAlloc, semispaceAlloc
	};
	
	/**
	 * Liefert Gesamtgröße des Heaps.
	 * @return Gesamtgröße des Heaps.
	 */
	public static int heapSize() {return HEAP_COLS * HEAP_ROWS;}
	
	/**
	 * Liefert Breite der Zeichenfläche.
	 * @return Breite der Zeichenfläche.
	 */
	public static int canvasWidth() { return HEAP_COLS * CELL_WIDTH + 2 * MARGIN_HORIZ;}
	
	/**
	 * Liefert Höhe der Zeichenfläche.
	 * @return Höhe der Zeichenfläche.
	 */
	public static int canvasHeight() { return HEAP_ROWS * CELL_HEIGHT + 2 * MARGIN_VERT;}
	
	/**
	 * Methode zur Sicherung der Einstellungen. Die Einstellungen werden in eine Datei {@code gcsim.config}
	 * geschrieben und beim nächsten Start der Anwendung ausgelesen.
	 * @return Fehlermeldung im Fehlerfall
	 */
	public static String writeSettings() {
		Launcher.log("Schreibe Einstellungen...", false);
		Properties settings = new Properties();
		settings.setProperty("HeapCols", "" + HEAP_COLS);
		settings.setProperty("HeapRows", "" + HEAP_ROWS);
		settings.setProperty("AnimationSpeed", "" + ANIMATION_SPEED);
		settings.setProperty("MinObjectSize", "" + MIN_OBJECT_SIZE);
		settings.setProperty("MaxObjectSize", "" + MAX_OBJECT_SIZE);
		settings.setProperty("Connectivity", "" + CONNECTIVITY);
		settings.setProperty("RootProb", "" + ROOT_PROBABILITY);
		settings.setProperty("RefDeletion", "" + REFERENCE_DELETION);
		settings.setProperty("CellWidth", "" + CELL_WIDTH);
		settings.setProperty("CellHeight", "" + CELL_HEIGHT);
		settings.setProperty("LastSelection", "" + LAST_SELECTION);
		
		try {
			settings.store(new FileOutputStream("gcsim.config"), "Garbage Collection Simulator Settings");
		} catch (IOException e) {
			String msg = "Fehler beim Sichern der Simulatoreinstellungen:\n" + e.getMessage();
			Launcher.log(msg, true);
			return msg;
		}
		return null;
	}
	
	/**
	 * Methode zum Auslesen der Einstellungen. Übernimmt die Einstellungen, die zuvor in die Datei {@code gcsim
	 * .config} geschrieben wurden.
	 */
	public static void readSettings() {
		Launcher.log("Lese Einstellungen...", false);
		if (new File("gcsim.config").isFile()) {
			Properties settings = new Properties();
			try {
				settings.load(new FileInputStream("gcsim.config"));
				int newHeapCols = Integer.parseInt(settings.getProperty("HeapCols"));
				int newHeapRows = Integer.parseInt(settings.getProperty("HeapRows"));
				if (newHeapCols < 10 || newHeapCols > 200 || newHeapRows < 10 || newHeapRows > 200)
					throw new IllegalArgumentException("Ungültige Größe für Heap.");
				int newAnimationSpeed = Integer.parseInt(settings.getProperty("AnimationSpeed"));
				if (newAnimationSpeed < 1 || newAnimationSpeed > 1000)
					throw new IllegalArgumentException("Ungültige Animationsgeschwindigkeit.");
				int newMinObjSize = Integer.parseInt(settings.getProperty("MinObjectSize"));
				int newMaxObjSize = Integer.parseInt(settings.getProperty("MaxObjectSize"));
				if (newMinObjSize < 1 || newMaxObjSize < 1 || newMinObjSize > newMaxObjSize)
					throw new IllegalArgumentException("Ungültige Größe für Objekte.");
				double newConnectivity = Double.parseDouble(settings.getProperty("Connectivity"));
				if (newConnectivity < 0 || newConnectivity > 1)
					throw new IllegalArgumentException("Ungültiger Verzweigungsgrad.");
				double newRootProb = Double.parseDouble(settings.getProperty("RootProb"));
				if (newRootProb < 0 || newRootProb > 1)
					throw new IllegalArgumentException("Ungültige Wahrscheinlichkeit für Basisobjekte.");
				double newDeletion = Double.parseDouble(settings.getProperty("RefDeletion"));
				if (newDeletion < 0 || newDeletion > 1)
					throw new IllegalArgumentException("Ungültiger Verwaisungsgrad.");
				int newCellWidth = Integer.parseInt(settings.getProperty("CellWidth"));
				int newCellHeight = Integer.parseInt(settings.getProperty("CellHeight"));
				if (newCellWidth < 5 || newCellWidth > 50 || newCellHeight < 5 || newCellHeight > 50)
					throw new IllegalArgumentException("Ungültige Größe für Heapblöcke.");
				int newSelection = Integer.parseInt(settings.getProperty("LastSelection"));
				if (newSelection < 0 || newSelection >= collectors.length)
					throw new IllegalArgumentException("Ungültiger Index für letzte GC-Auswahl.");
				HEAP_COLS = newHeapCols;
				HEAP_ROWS = newHeapRows;
				ANIMATION_SPEED = newAnimationSpeed;
				MIN_OBJECT_SIZE = newMinObjSize;
				MAX_OBJECT_SIZE = newMaxObjSize;
				CONNECTIVITY = newConnectivity;
				ROOT_PROBABILITY = newRootProb;
				REFERENCE_DELETION = newDeletion;
				CELL_WIDTH = newCellWidth;
				CELL_HEIGHT = newCellHeight;
				LAST_SELECTION = newSelection;
			} catch (IOException | IllegalArgumentException e) {
				Launcher.log("Fehler beim Laden der Simulatoreinstellungen:\n" + e.getMessage(), true);
				Launcher.log("Verwende Standardeinstellungen.", false);
			}
		} else Launcher.log("Keine Konfiguration gefunden. Verwende Standardeinstellungen.", false);
	}
}