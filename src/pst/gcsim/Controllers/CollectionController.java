/*
 * Copyright 2019 Phil Steinhorst
 *
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package pst.gcsim.Controllers;

import pst.gcsim.Allocators.Allocator;
import pst.gcsim.GUI.*;
import pst.gcsim.GarbageCollectors.GarbageCollector;
import pst.gcsim.Launcher;
import pst.gcsim.Objects.HeapObject;
import pst.gcsim.Settings;

import javax.swing.*;
import java.util.ArrayList;
import java.util.concurrent.ThreadLocalRandom;

/**
 * Garbage-Collection-Steuerklasse, die Allokator und Kollektor verwaltet.
 *
 * @author Phil Steinhorst
 */
public class CollectionController<T extends Allocator> {
	/**
	 * Allokator zur Erzeugung und Freigabe von Heapobjekten.
	 */
	private T alloc;
	
	/**
	 * Garbage Collector.
	 */
	private GarbageCollector<T> gc;
	
	/**
	 * Anknüpfung an das Steuerformular.
	 */
	private ControlView ctrlView;
	
	/**
	 * Anknüpfung an Zeichenfläche.
	 */
	private CanvasView canvas;
	
	/**
	 * Menge der Heapobjekte.
	 */
	private ArrayList<HeapObject> objects;
	
	/**
	 * Menge der Basisobjekte.
	 */
	private ArrayList<HeapObject> roots;
	
	/**
	 * Swing-Timer für Animationen in nachvollziehbarer Geschwindigkeit.
	 */
	private Timer timer;
	
	/**
	 * Konstruktor zur Erzeugung einer Instanz dieser Steuerklasse.
	 *
	 * @param testMode Erzeugt einen Dummy-Controller ohne GUI, Allokator und Mengen für Tests, wenn {@code true}.
	 *                 Normaler Betrieb, wenn {@code false}.
	 * @param alloc    zu verwendender Allokator.
	 * @param gc       zu verwendende Garbage Collection.
	 */
	public CollectionController(boolean testMode, T alloc, GarbageCollector<T> gc) {
		String title = "";
		objects = new ArrayList<>();
		roots = new ArrayList<>();
		this.alloc = alloc;
		this.gc = gc;
		if (gc != null) {
			gc.initialize(this);
			title = gc.getName();
		}
		if (!testMode) {
			ctrlView = new ControlView(title, this);
			canvas = new CanvasView(objects);
		}
	}
	
	/**
	 * Methode zur Erzeugung eines zufälligen Objekts.
	 */
	public void createObject() {
		// zufällige Größe
		int size = ThreadLocalRandom.current().nextInt(Settings.MIN_OBJECT_SIZE, Settings.MAX_OBJECT_SIZE + 1);
		HeapObject obj = alloc.newObject(size);
		if (obj == null) {
			ctrlView.setStatus("Allokation fehlgeschlagen: Nicht genügend Speicher (Objektgröße: " + size + ").",
			                   true);
			return;
		}
		objects.add(obj);
		integrateObject(obj);         // Objekt mit anderen Objekten verknüpfen
		refreshView();
		ctrlView.setStatus("Objekt der Größe " + obj.getSize() + " hinzugefügt.", false);
	}
	
	/**
	 * Verknüpfung eines Objekts im Heap durch zufälliges Hinzufügen zu {@code roots} bzw. {@code pointers}.
	 *
	 * @param obj Neu erzeugtes Objekt.
	 */
	private void integrateObject(HeapObject obj) {
		if (ThreadLocalRandom.current().nextDouble() <= Settings.ROOT_PROBABILITY) roots.add(obj);
		for (HeapObject src : objects)
			if (ThreadLocalRandom.current().nextDouble() <= Settings.CONNECTIVITY) src.getPointers().add(obj);
	}
	
	/**
	 * Methode zur Aktualisierung des Canvas.
	 */
	public void refreshView() {
		if (canvas != null && ctrlView.isVisible()) {
			canvas.refreshFrame(alloc.getPosition());
		}
	}
	
	/**
	 * Methode zum Auffüllen des Heaps.
	 */
	public void fillHeap() {
		ctrlView.setStatus("Fülle Heap mit Objekten auf...", false);
		ctrlView.setInputs(false);          // GUI vorübergehend deaktivieren
		timer = new Timer(Settings.ANIMATION_SPEED, action -> {
			HeapObject obj = alloc.newObject(ThreadLocalRandom.current().nextInt(Settings.MIN_OBJECT_SIZE,
			                                                                     Settings.MAX_OBJECT_SIZE + 1));
			if (obj != null) {
				objects.add(obj);
				integrateObject(obj);
				refreshView();
			} else {
				timer.stop();                   // Timer anhalten, wenn Allokation fehlschlägt
				ctrlView.setInputs(true);
				ctrlView.setStatus("Heap aufgefüllt.", false);
			}
		});
		timer.setRepeats(true);
		timer.start();              // starte erzeugten Timer
	}
	
	/**
	 * Methode zur Auslösung der Garbage Collection.
	 */
	public void collectGarbage() {
		Launcher.log("Starte Garbage Collection...", false);
		ctrlView.setInputs(false);
		killRoots();
		killReferences();
		gc.collect(true);
	}
	
	/**
	 * Methode zur zufälligen Entfernung von Objekten aus {@code roots}.
	 */
	private void killRoots() {
		// Führe Löschliste zur Vermeidung von Nebenläufigkeitskonflikten
		ArrayList<HeapObject> toRemove = new ArrayList<>();
		for (HeapObject obj : roots)
			if (ThreadLocalRandom.current().nextDouble() <= Settings.REFERENCE_DELETION) toRemove.add(obj);
		toRemove.forEach(obj -> roots.remove(obj));
	}
	
	/**
	 * Methode zur zufälligen Entfernung von Objekten aus {@code pointers}.
	 */
	private void killReferences() {
		// Führe Löschliste zur Vermeidung von Nebenläufigkeitskonflikten
		ArrayList<HeapObject> toRemove = new ArrayList<>();
		for (HeapObject obj : objects) {
			toRemove.clear();
			for (HeapObject ref : obj.getPointers())
				if (ThreadLocalRandom.current().nextDouble() <= Settings.REFERENCE_DELETION) toRemove.add(ref);
			toRemove.forEach(ref -> obj.getPointers().remove(ref));
		}
	}
	
	/**
	 * Methode zum Leeren des Heaps.
	 */
	public void clearHeap() {
		objects.clear();
		roots.clear();
		alloc.reset();
		refreshView();
		setStatus("Heap geleert.", false);
	}
	
	/**
	 * Übermittelt zu setzenden Status an das Steuerformular.
	 *
	 * @param msg      Nachricht
	 * @param critical {@code true}, wenn kritischer Fehler, sonst {@code false}.
	 */
	public void setStatus(String msg, boolean critical) {
		if (ctrlView != null)
			ctrlView.setStatus(msg, critical);
	}
	
	/**
	 * Bricht aktuelle Aktion ab.
	 */
	public void cancel() {
		if (timer != null)
			timer.stop();
		if (gc != null && gc.getTimer() != null)
			gc.getTimer().stop();
	}
	
	/**
	 * Liefert zurück, ob die Zeichenfläche sichtbar ist.
	 *
	 * @return {@code true}, falls Zeichenfläche sichtbar, sonst {@code false}.
	 */
	public boolean getCanvasVisibility() {
		return canvas.isVisible();
	}
	
	/**
	 * Setzt Sichtbarkeit der Zeichenfläche.
	 *
	 * @param visibility {@code true}: Zeichenfläche einblenden, {@code false}: Zeichenfläche ausblenden.
	 */
	public void setCanvasVisibility(boolean visibility) {
		canvas.setVisible(visibility);
	}
	
	/**
	 * Schließt Kontrollfenster und Zeichenfläche und ruft den Auswahldialog auf.
	 */
	public void exit() {
		Launcher.log("Beende Simulation...", false);
		canvas.dispose();
		ctrlView.dispose();
		new GcSelectionView();
	}
	
	/**
	 * Übermittelt (De-)Aktivierung der Eingabekomponenten an das Steuerformular.
	 *
	 * @param state {@code true}: Aktiviert alle Komponenten außer dem Abbruchbutton.
	 *              {@code false}: Aktiviert lediglich den Abbruchbutton.
	 */
	public void setInputs(boolean state) {
		if (ctrlView != null)
			ctrlView.setInputs(state);
	}
	
	/**
	 * Gibt zurück, ob eine Garbage Collection verwendet wird.
	 *
	 * @return {@code true}, falls GC verwendet wird, sonst {@code false}.
	 */
	public boolean hasCollector() {
		return (gc != null);
	}
	
	/**
	 * Liefert den verwalteten Allokator.
	 *
	 * @return Attribut {@code alloc}.
	 */
	public T getAllocator() {
		return alloc;
	}
	
	/**
	 * Liefert die Menge aller Heapobjekte.
	 *
	 * @return Attribut {@code objects}.
	 */
	public ArrayList<HeapObject> getObjects() {
		return objects;
	}
	
	/**
	 * Liefert die Menge der Basisobjekte.
	 *
	 * @return Attribut {@code roots}.
	 */
	public ArrayList<HeapObject> getRoots() { return roots; }
}